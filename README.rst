========
Overview
========

PowerFun is a follow-up project to the various works on the realization of the intestinal microbiota system. The
objective of the project is to determine which nutrients are necessary for the system to produce the components of
interest.

* Free software: GNU Lesser General Public License v3 or later (LGPLv3+)

Installation
============

::

    pip install powerfun

You can also install the in-development version with::

    pip install git+ssh://git@gitlab.inria.fr/pleiade/powerfun.git@main

Documentation
=============


https://powerfun.readthedocs.io/


Development
===========

To run all the tests run::

    tox

Note, to combine the coverage data from all the tox environments run:

.. list-table::
    :widths: 10 90
    :stub-columns: 1

    - - Windows
      - ::

            set PYTEST_ADDOPTS=--cov-append
            tox

    - - Other
      - ::

            PYTEST_ADDOPTS=--cov-append tox


environments
- pandas
- clyngor with clingo
- clingo
- miscoto
- bubbletools
- padmet


```
############ POWERFUN ##############
powerfun bconcat -b data/sbml/ -od temp/ -of test_all_bact.lp -c 1

powerfun reaction -b data/toys/fictive_network/Complex_3_PN/toy_sbml/ -s data/toys/fictive_network/toy_seeds.sbml -p data/toys/fictive_network/Complex_3_PN/toy_powergraph.bbl -gt data/toys/fictive_network/genometax.tsv -tp temp/  -ta data/toys/fictive_network/toy_targets.tsv

powerfun metabolite -b data/toys/fictive_network/Complex_3_PN/toy_sbml/ -s data/toys/fictive_network/toy_seeds.sbml -p data/toys/fictive_network/Complex_3_PN/toy_powergraph.bbl -gt data/toys/fictive_network/genometax.tsv -tp temp/ -ta data/toys/fictive_network/toy_targets.tsv

powerfun metareact -b data/toys/fictive_network/Complex_3_PN/toy_sbml/ -s data/toys/fictive_network/toy_seeds.sbml -p data/toys/fictive_network/Complex_3_PN/toy_powergraph.bbl -gt data/toys/fictive_network/genometax.tsv -tp temp/  -ta data/toys/fictive_network/toy_targets.tsv

powerfun analysis -b data/toys/fictive_network/Complex_3_PN/toy_sbml/ -s data/toys/fictive_network/toy_seeds.sbml -p data/toys/fictive_network/Complex_3_PN/toy_powergraph.bbl -gt data/toys/fictive_network/genometax.tsv -tp temp/ -od result/ -ta data/toys/fictive_network/toy_targets.tsv

powerfun analysis -b data/toys/fictive_network/Complex_3_PN/toy_sbml/ -s data/toys/fictive_network/toy_seeds.sbml -p data/toys/fictive_network/Complex_3_PN/toy_powergraph.bbl -gt data/toys/fictive_network/genometax.tsv -tp temp/ -od result/ -ta data/toys/fictive_network/toy_targets.tsv

powerfun analysis -b data/toys/fictive_network/Complex_3_PN_loop/toy_sbml/ -s data/toys/fictive_network/toy_seeds.sbml -p data/toys/fictive_network/Complex_3_PN_loop/toy_powergraph.bbl -gt data/toys/fictive_network/genometax.tsv -tp temp/ -od result/ -ta data/toys/fictive_network/toy_targets.tsv

powerfun bconcat -b data/toys/fictive_network/Complex_3_PN/toy_sbml/ -od temp/ -of toy_sbml_all_bact_instances.lp -gt data/toys/fictive_network/genometax.tsv  -c 1

powerfun convert_analysis -j result/toy_powergraph_analysis_results.json -od result/

powerfun phylum -b data/toys/fictive_network/Complex_3_PN_loop/toy_sbml/ -s data/toys/fictive_network/toy_seeds.sbml -p data/toys/fictive_network/Complex_3_PN_loop/toy_powergraph.bbl -gt data/toys/fictive_network/genometax.tsv -tp temp/ -od result/ -ta data/toys/fictive_network/toy_targets.tsv

################# ###################




############ CLINGO ##############
clingo  -n 0 data/toys/fictive_network/Complex_3_PN/all_bacterias.lp  data/toys/fictive_network/Complex_3_PN/toy_powergraph.lp src/powerfun/encodings/selection_analysis.lp data/toys/fictive_network/toy_seeds.lp data/toys/fictive_network/toy_targets.lp

clingo  -n 0 data/toys/fictive_network/Complex_3_PN/all_bacterias.lp  data/toys/fictive_network/Complex_3_PN/toy_powergraph.lp src/powerfun/encodings/selection_analysis.lp data/toys/fictive_network/toy_seeds.lp data/toys/fictive_network/toy_targets.lp src/powerfun/encodings/metabolite_analysis.lp

clingo  -n 0 data/toys/fictive_network/Complex_3_PN/all_bacterias.lp  data/toys/fictive_network/Complex_3_PN/toy_powergraph.lp src/powerfun/encodings/selection_analysis.lp data/toys/fictive_network/toy_seeds.lp data/toys/fictive_network/toy_targets.lp src/powerfun/encodings/reaction_analysis.lp

clingo  -n 0 data/toys/fictive_network/Complex_3_PN/all_bacterias.lp  data/toys/fictive_network/Complex_3_PN/toy_powergraph.lp src/powerfun/encodings/selection_analysis.lp data/toys/fictive_network/toy_seeds.lp data/toys/fictive_network/toy_targets.lp src/powerfun/encodings/mr_analysis.lp



clingo  -n 0 data/toys/fictive_network/Complex_3_PN/all_bacterias.lp  data/toys/fictive_network/Complex_3_PN/toy_powergraph.lp src/powerfun/encodings/selection_analysis.lp data/toys/fictive_network/toy_seeds.lp data/toys/fictive_network/toy_targets.lp src/powerfun/encodings/phylum_analysis.lp
################# ###################



############ MISCOTO ##############
miscoto mincom -b data/toys/fictive_network/Complex_3_PN_loop/toy_sbml/ -s  data/toys/fictive_network/toy_seeds.sbml -t data/toys/fictive_network/toy_targets.sbml --optsol --enumeration --intersection --union -o soup

miscoto mincom -b data/toys/fictive_network/Complex_3_PN_loop/toy_sbml/ -s  data/toys/fictive_network/toy_seeds.sbml -t data/toys/fictive_network/toy_targets.sbml --optsol --enumeration --intersection --union -o minexch

miscoto focus --all --output focus_sol2.json -s toy_seeds.sbml -b sol2/
################# ###################
```