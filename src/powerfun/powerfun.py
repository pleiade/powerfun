import os
import logging
import time
import sys
import shutil
from powerfun import files, query, commons
from miscoto import utils as m_utils

logger = logging.getLogger(__name__)

def create_concat_bact(bacteria_dir, genotax_tsv_file, tmp_dir=None, all_bact_file_name=None, cpu=1, is_force=False):
    path_all_bact_file = os.path.join(*[tmp_dir, all_bact_file_name])

    if (not os.path.exists(path_all_bact_file)) or is_force:
        start_time = time.time()
        logger.info("\t-> Creating all bacterias file")
        if not tmp_dir:
            if not os.path.isdir(commons.DIR_POWERFUN):
                os.mkdir(commons.DIR_POWERFUN)
            tmp_dir = commons.DIR_TEMP
        tmp_species_dir= os.path.join(*[tmp_dir ,'all_bacteria'])

        if not os.path.isdir(tmp_species_dir):
            os.mkdir(tmp_species_dir)
        files.write_concat_sbml_asp_file(bacteria_dir, genotax_tsv_file, tmp_species_dir, cpu)
        with open(path_all_bact_file, 'wb') as outfile:
            for species in os.listdir(tmp_species_dir):
                species_path = os.path.join(*[tmp_species_dir, species])
                with open(species_path, 'rb') as readfile:
                    shutil.copyfileobj(readfile, outfile)
        logger.info("%s seconds" % (time.time() - start_time))

        logger.info("\t-> Cleaning bacteria directory temp...")
        start_time = time.time()
        commons.clean_up_dir(tmp_species_dir)
        logger.info("%s seconds" % (time.time() - start_time))
    return path_all_bact_file


def run_analysis(analyse_type, seeds_sbml_file, bacteria_dir, 
                pg_bbl_file, genotax_tsv_file, path_all_bact_file, targets_tsv_file=None, 
                tmp_dir=None, out_dir=None, output_json=None):
    """
    """
    start_time = time.time()

    if not tmp_dir:
        if not os.path.isdir(commons.DIR_POWERFUN):
            os.mkdir(commons.DIR_POWERFUN)
        tmp_dir = commons.DIR_TEMP
    if not os.path.isdir(tmp_dir):
        os.mkdir(tmp_dir)

    if not out_dir:
        if not os.path.isdir(commons.DIR_POWERFUN):
            os.mkdir(commons.DIR_POWERFUN)
        out_dir = commons.DIR_RESULT
    if not os.path.isdir(out_dir):
        os.mkdir(out_dir)

    if not os.path.isdir(bacteria_dir):
        logger.info("Symbiont directory not found")
        sys.exit(1)    

    if not os.path.isfile(seeds_sbml_file):
            logger.info('Seeds file not found')
            sys.exit(1)

    if not os.path.isfile(pg_bbl_file):
            logger.info('Powergraph file not found')
            sys.exit(1)
    
    if not os.path.isfile(genotax_tsv_file):
            logger.info('Genome-Taxonomy file not found')
            sys.exit(1)


    pg_file_name = commons.basename(pg_bbl_file)
    tmp_file_name = pg_file_name + "_all_instances" + commons.ASP_FILE_EXTENSION
    lp_instance_file =  os.path.join(*[tmp_dir, tmp_file_name])

    if not output_json:
        output_json = pg_file_name + "_" + analyse_type + "_results" + commons.JSON_FILE_EXTENSION  
    
    logger.info("--- " + pg_file_name + " ---")
    logger.info("\t-> Reading files...")
    shutil.copy(path_all_bact_file, lp_instance_file)
    lp_instance_file = files.write_sbml_asp_file(seeds_sbml_file, tmp_dir, tmp_file_name)
    lp_instance_file = files.write_bbl_asp_file(pg_bbl_file, genotax_tsv_file, tmp_dir, tmp_file_name)
    lp_instance_file = files.write_tsv_asp_target_file(targets_tsv_file, tmp_dir, tmp_file_name)
    
    logger.info("%s seconds" % (time.time() - start_time))
    
    results = compute(lp_instance_file, analyse_type)

    logger.info("\t-> Writing file...")
    result_path = os.path.join(*[out_dir, output_json])
    m_utils.to_json(results, result_path)
    logger.info("Result file: " + result_path)
    
    logger.info("\t-> Cleaning temp...")
    commons.clean_up_file(tmp_dir, tmp_file_name)


def compute(lp_instance_file, analyse_type):
    start_time = time.time()
    logger.info("\t-> Computing analysis...")

    results = {}
    models = query.get_models(lp_instance_file, analyse_type)
  
    for it, pred in enumerate(models):
        powernode = []
        #set_reaction_only_all_selected_outside = set()
        #reaction_only_all_selected_outside = []
        #set_reaction_only_all_selected_outside_activ = set()
        #reaction_only_all_selected_outside_activ = []
        set_reaction_only_all_selected = set()
        reaction_only_all_selected = []
        set_reaction_only_all_selected_activ = set()
        reaction_only_all_selected_activ = []
        nb_transfered_prod_by_all_selected = []
        producible_only_all_selected = []
        transfered_prod_only_all_selected = {}
        produce_target_selected = []
        produce_target_all_selected = []
        produce_target_only_all_selected = []
        #target_poducible_pg = []
        #target = []
        #mr_only_all_selected_outside = {}
        #mr_activ_only_all_selected_outside = {}
        mr_only_all_selected = {}
        mr_activ_only_all_selected = {}
        genus_all_selected = []
        family_all_selected = []
        order_all_selected = []
        class_all_selected = []
        phyl_all_selected = []
        same_slct_to_bbl_genus = []
        same_slct_to_bbl_family = []
        same_slct_to_bbl_order = []
        same_slct_to_bbl_class = []
        same_slct_to_bbl_phyl = []
        same_slct_to_outside_genus = []
        same_slct_to_outside_family = []
        same_slct_to_outside_order = []
        same_slct_to_outside_class = []
        same_slct_to_outside_phyl = []
        for result in pred:
            match  result[0]:
                case "selected_pwrn":
                    powernode.append(result[1][0].replace("\"","") + " / " + result[1][1].replace("\"",""))
                    
                case "reaction_only_all_selected":
                    id_reaction=str(result[1][0].replace("\"",""))
                    if("_rev" in id_reaction):
                        id_reaction = id_reaction.rsplit("_",1)[0]
                        if(id_reaction not in set_reaction_only_all_selected):
                            set_reaction_only_all_selected.add(id_reaction)
                    else:
                        set_reaction_only_all_selected.add(id_reaction)
                    
                case "reaction_only_all_selected_activ":
                    id_reaction=str(result[1][0].replace("\"",""))
                    if("_rev" in id_reaction):
                        id_reaction = id_reaction.rsplit("_",1)[0]
                        if(id_reaction not in set_reaction_only_all_selected_activ):
                            set_reaction_only_all_selected_activ.add(id_reaction)
                    else:
                        set_reaction_only_all_selected_activ.add(id_reaction)

                #case "reaction_only_all_selected_outside":
                #    id_reaction=str(result[1][0].replace("\"",""))
                #    if("_rev" in id_reaction):
                #        id_reaction = id_reaction.rsplit("_",1)[0]
                #        if(id_reaction not in set_reaction_only_all_selected_outside):
                #            set_reaction_only_all_selected_outside.add(id_reaction)
                #    else:
                #        set_reaction_only_all_selected_outside.add(id_reaction)
                    
                #case "reaction_only_all_selected_outside_activ":
                #    id_reaction=str(result[1][0].replace("\"",""))
                #    if("_rev" in id_reaction):
                #        id_reaction = id_reaction.rsplit("_",1)[0]
                #        if(id_reaction not in set_reaction_only_all_selected_outside_activ):
                #            set_reaction_only_all_selected_outside_activ.add(id_reaction)
                #    else:
                #        set_reaction_only_all_selected_outside_activ.add(id_reaction)
                    
                case "nb_transfered_prod_by_all_selected":
                    nb_transfered_prod_by_all_selected.append(result[1][0])

                case "producible_only_all_selected":
                    producible_only_all_selected.append(result[1][0].replace("\"",""))
                        
                case "transfered_prod_only_all_selected":
                    meta = str(result[1][0].replace("\"",""))
                    if meta not in transfered_prod_only_all_selected:
                        transfered_prod_only_all_selected[meta]=[result[1][1].replace("\"","")]
                    else:
                        transfered_prod_only_all_selected[meta].append(result[1][1].replace("\"",""))
                    
                case "produce_target_selected":
                    produce_target_selected.append(result[1][0].replace("\"",""))
                    
                case "produce_target_all_selected":
                    produce_target_all_selected.append(result[1][0].replace("\"",""))
                    
                case "produce_target_only_all_selected":
                    produce_target_only_all_selected.append(result[1][0].replace("\"",""))

                
                #case "target_poducible_pg":
                #    target_poducible_pg.append(result[1][0].replace("\"",""))

                #case "target":
                #    target.append(result[1][0].replace("\"",""))

                    
                case "mr_only_all_selected":
                    id_reaction=str(result[1][1].replace("\"",""))
                    if("_rev" in id_reaction):
                        id_reaction = id_reaction.rsplit("_",1)[0]
                    if id_reaction not in mr_only_all_selected:
                        mr_only_all_selected[id_reaction]=[result[1][0].replace("\"","")]
                    else:
                        mr_only_all_selected[id_reaction].append(result[1][0].replace("\"",""))
                        
                case "mr_activ_only_all_selected":
                    id_reaction=str(result[1][1].replace("\"",""))
                    if("_rev" in id_reaction):
                        id_reaction = id_reaction.rsplit("_",1)[0]
                    if id_reaction not in mr_activ_only_all_selected:
                        mr_activ_only_all_selected[id_reaction]=[result[1][0].replace("\"","")]  
                    else:
                        mr_activ_only_all_selected[id_reaction].append(result[1][0].replace("\"",""))
                
                #case "mr_only_all_selected_outside":
                #    id_reaction=str(result[1][1].replace("\"",""))
                #    if("_rev" in id_reaction):
                #        id_reaction = id_reaction.rsplit("_",1)[0]
                #    if id_reaction not in mr_only_all_selected_outside:
                #        mr_only_all_selected_outside[id_reaction]=[result[1][0].replace("\"","")]
                #    else:
                #        mr_only_all_selected_outside[id_reaction].append(result[1][0].replace("\"",""))
                        
                #case "mr_activ_only_all_selected_outside":
                #    id_reaction=str(result[1][1].replace("\"",""))
                #    if("_rev" in id_reaction):
                #        id_reaction = id_reaction.rsplit("_",1)[0]
                #    if id_reaction not in mr_activ_only_all_selected_outside:
                #        mr_activ_only_all_selected_outside[id_reaction]=[result[1][0].replace("\"","")]  
                #    else:
                #        mr_activ_only_all_selected_outside[id_reaction].append(result[1][0].replace("\"",""))

                case "genus_all_selected":
                   genus_all_selected.append(result[1][0].replace("\"",""))
                case "family_all_selected":
                   family_all_selected.append(result[1][0].replace("\"",""))
                case "order_all_selected":
                   order_all_selected.append(result[1][0].replace("\"",""))
                case "class_all_selected":
                   class_all_selected.append(result[1][0].replace("\"",""))
                case "phyl_all_selected":
                   phyl_all_selected.append(result[1][0].replace("\"",""))

                case "same_slct_to_bbl_genus":
                   same_slct_to_bbl_genus.append(result[1][1].replace("\"","") + " / " + \
                                                result[1][0].replace("\"",""))
                case "same_slct_to_bbl_family":
                   same_slct_to_bbl_family.append(result[1][2].replace("\"","") + " / " + \
                                                result[1][1].replace("\"","") + " / " + \
                                                result[1][0].replace("\"",""))
                case "same_slct_to_bbl_order":
                   same_slct_to_bbl_order.append(result[1][3].replace("\"","") + " / " + \
                                                result[1][2].replace("\"","") + " / " +  \
                                                result[1][1].replace("\"","") + " / "+\
                                                result[1][0].replace("\"",""))
                case "same_slct_to_bbl_class":
                   same_slct_to_bbl_class.append(result[1][4].replace("\"","") + " / " + \
                                                result[1][3].replace("\"","") + " / " +  \
                                                result[1][2].replace("\"","") + " / "+ \
                                                result[1][1].replace("\"","") + " / " +  \
                                                result[1][0].replace("\"",""))
                case "same_slct_to_bbl_phyl":
                   same_slct_to_bbl_phyl.append(result[1][5].replace("\"","") + " / " + \
                                                result[1][4].replace("\"","") + " / " + \
                                                result[1][3].replace("\"","") + " / " +  \
                                                result[1][2].replace("\"","") + " / "+ \
                                                result[1][1].replace("\"","") + " / " +  \
                                                result[1][0].replace("\"",""))

                case "same_slct_to_outside_genus":
                   same_slct_to_outside_genus.append(result[1][0].replace("\"",""))
                case "same_slct_to_outside_family":
                   same_slct_to_outside_family.append(result[1][1].replace("\"","") + " / " + \
                                                    result[1][0].replace("\"",""))
                case "same_slct_to_outside_order":
                   same_slct_to_outside_order.append(result[1][2].replace("\"","") + " / " + \
                                                    result[1][1].replace("\"","") + " / " + \
                                                    result[1][0].replace("\"",""))
                case "same_slct_to_outside_class":
                   same_slct_to_outside_class.append(result[1][3].replace("\"","") + " / " + \
                                                    result[1][2].replace("\"","") + " / " + \
                                                    result[1][1].replace("\"","") + " / " + \
                                                    result[1][0].replace("\"",""))
                case "same_slct_to_outside_phyl":
                   same_slct_to_outside_phyl.append(result[1][4].replace("\"","") + " / " + \
                                                    result[1][3].replace("\"","") + " / " + \
                                                    result[1][2].replace("\"","") + " / " + \
                                                    result[1][1].replace("\"","") + " / " + \
                                                    result[1][0].replace("\"",""))
                                                    

                    
        reaction_only_all_selected = list(set_reaction_only_all_selected)
        reaction_only_all_selected_activ = list(set_reaction_only_all_selected_activ)
        #reaction_only_all_selected_outside = list(set_reaction_only_all_selected_outside)
        #reaction_only_all_selected_outside_activ = list(set_reaction_only_all_selected_outside_activ)

        results['model_'+str(it)] = ["powernode", powernode]
        match  analyse_type:
            case "reaction":
                results['model_'+str(it)] = results['model_'+str(it)] + \
                        ["reaction_only_all_selected", reaction_only_all_selected] + \
                        ["reaction_only_all_selected_activ", reaction_only_all_selected_activ] 
                        #["reaction_only_all_selected_outside", reaction_only_all_selected_outside] + \
                        #["reaction_only_all_selected_outside_activ", reaction_only_all_selected_outside_activ] + \
            case "metabolite":
                results['model_'+str(it)] = results['model_'+str(it)] + \
                        ["producible_only_all_selected", producible_only_all_selected] + \
                        ["nb_transfered_prod_by_all_selected", nb_transfered_prod_by_all_selected] + \
                        ["transfered_prod_only_all_selected", transfered_prod_only_all_selected] + \
                        ["produce_target_selected", produce_target_selected] + \
                        ["produce_target_all_selected", produce_target_all_selected] + \
                        ["produce_target_only_all_selected", produce_target_only_all_selected] #+ \
                        #["target_poducible_pg", target_poducible_pg] + \
                        #["target", target]
            case "metareact":
                results['model_'+str(it)] = results['model_'+str(it)] + \
                        ["mr_only_all_selected", mr_only_all_selected]+ \
                        ["mr_activ_only_all_selected", mr_activ_only_all_selected] #+ \
                        #["mr_only_all_selected_outside", mr_only_all_selected_outside]+ \
                        #["mr_activ_only_all_selected_outside", mr_activ_only_all_selected_outside]
            case "phylum":
                results['model_'+str(it)] = results['model_'+str(it)] + \
                        ["genus_all_selected", genus_all_selected]+ \
                        ["family_all_selected", family_all_selected] + \
                        ["order_all_selected", order_all_selected] + \
                        ["class_all_selected", class_all_selected] + \
                        ["phyl_all_selected", phyl_all_selected] + \
                        ["same_slct_to_bbl_genus", same_slct_to_bbl_genus] + \
                        ["same_slct_to_bbl_family", same_slct_to_bbl_family] + \
                        ["same_slct_to_bbl_order", same_slct_to_bbl_order] + \
                        ["same_slct_to_bbl_class", same_slct_to_bbl_class] + \
                        ["same_slct_to_bbl_phyl", same_slct_to_bbl_phyl] + \
                        ["same_slct_to_outside_genus", same_slct_to_outside_genus] + \
                        ["same_slct_to_outside_family", same_slct_to_outside_family] + \
                        ["same_slct_to_outside_order", same_slct_to_outside_order]+ \
                        ["same_slct_to_outside_class", same_slct_to_outside_class]+ \
                        ["same_slct_to_outside_phyl", same_slct_to_outside_phyl]
            case "analysis":
                results['model_'+str(it)] = results['model_'+str(it)] + \
                        ["reaction_only_all_selected", reaction_only_all_selected] + \
                        ["reaction_only_all_selected_activ", reaction_only_all_selected_activ] + \
                        ["producible_only_all_selected", producible_only_all_selected] + \
                        ["nb_transfered_prod_by_all_selected", nb_transfered_prod_by_all_selected] + \
                        ["transfered_prod_only_all_selected", transfered_prod_only_all_selected] + \
                        ["produce_target_selected", produce_target_selected] + \
                        ["produce_target_all_selected", produce_target_all_selected] + \
                        ["produce_target_only_all_selected", produce_target_only_all_selected] + \
                        ["mr_only_all_selected", mr_only_all_selected] + \
                        ["mr_activ_only_all_selected", mr_activ_only_all_selected] # + \

                        #["reaction_only_all_selected_outside", reaction_only_all_selected_outside] + \
                        #["reaction_only_all_selected_outside_activ", reaction_only_all_selected_outside_activ] + \ 
                        #["mr_only_all_selected_outside", mr_only_all_selected_outside]+ \
                        #["mr_activ_only_all_selected_outside", mr_activ_only_all_selected_outside]
                        #["target_poducible_pg", target_poducible_pg] + \
                        #["target", target]
        
    logger.info("%s seconds" % (time.time() - start_time))
    return results