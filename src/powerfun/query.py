import clyngor
from powerfun import commons


def get_models(instance_f, analysis_type):
    """Get models from analysis (Reaction / Metabolite / Metabolite-Reaction)
    
    Args:
        instance_f (str) : ASP instance file
        encoding (str) : ASP model encoding
    
    Returns:
        TermSet: ASP model
    """
    match  analysis_type:
        case "reaction":
            prg = [commons.ASP_SRC_SELECTION, commons.ASP_SRC_REACTIONS, instance_f]
        case "metabolite":
            prg = [commons.ASP_SRC_SELECTION, commons.ASP_SRC_METABOLITES, instance_f]
        case "metareact":
            prg = [commons.ASP_SRC_SELECTION, commons.ASP_SRC_META_REACT, instance_f]
        case "analysis":
            prg = [commons.ASP_SRC_SELECTION, commons.ASP_SRC_REACTIONS, commons.ASP_SRC_METABOLITES, 
                    commons.ASP_SRC_META_REACT, instance_f]
        case "phylum":
            prg = [commons.ASP_SRC_SELECTION, commons.ASP_SRC_PHYLUM, instance_f]

    options = ''
    
    models = clyngor.solve(prg, options=options, use_clingo_module=False)
    return models