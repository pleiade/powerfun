"""

"""
import sys
import os
import argparse
import logging
import pkg_resources
import time


import powerfun.powerfun as pf
from powerfun import commons
from shutil import which

VERSION = pkg_resources.get_distribution("powerfun").version
LICENSE = """Copyright (C) Dyliss
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
powerfun is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.\n
"""
MESSAGE = """
Analyse functions within a powernode.
"""
REQUIRES = """
Requires Miscoto, Bubbletools, Clingo and clyngor package: "pip install clyngor clyngor-with-clingo"
"""

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

# Check ASP binaries.
if not which('clingo'):
    logger.critical('clingo is not in the Path, powerfun can not work without it.')
    logger.critical('You can install with: pip install clyngor-with-clingo')
    sys.exit(1)

# Check Miscoto binaries.
if not which('miscoto'):
    logger.critical('miscoto is not in the Path, powerfun can not work without it.')
    logger.critical('You can install with: pip install miscoto')
    sys.exit(1)
    
# Check Bubbletools binaries.
#if not which('bubbletools'):
#    logger.critical('bubbletools is not in the Path, powerfun can not work without it.')
#    logger.critical('You can install with: pip install bubbletools')
#    sys.exit(1)

logging.basicConfig(format='%(message)s', level=logging.DEBUG)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)



def main(argv=sys.argv):
    """Run programm.
    """
    start_time = time.time()
    parser = argparse.ArgumentParser(
        "powerfun",
        description=MESSAGE + " For specific help on each subcommand use: powerfun {cmd} --help",
        epilog=REQUIRES
    )
    parser.add_argument(
        "-v",
        "--version",
        action="version",
        version="%(prog)s " + VERSION + "\n" + LICENSE
        )
    # parent parser
    # Powerfun instance specific arguments.
    parent_parser_b = argparse.ArgumentParser(add_help=False)
    parent_parser_b.add_argument(
        "-b",
        "--bactsymbionts",
        dest="bactsymbionts",
        help="directory of symbionts models, all in sbml format",
        required=True,
    )
    parent_parser_s = argparse.ArgumentParser(add_help=False)
    parent_parser_s.add_argument(
        "-s",
        "--seeds",
        dest="seeds",
        help="seeds file, sbml file",
        required=True,
    )
    parent_parser_p = argparse.ArgumentParser(add_help=False)
    parent_parser_p.add_argument(
        "-p",
        "--powergraph",
        dest="powergraph",
        help="powergraph file, bbl file",
        required=True,
    )
    parent_parser_gt = argparse.ArgumentParser(add_help=False)
    parent_parser_gt.add_argument(
        "-gt",
        "--genometax",
        dest="genometax",
        help="genome-taxonomy file, tsv file",
        required=True,
    )
    parent_parser_ta = argparse.ArgumentParser(add_help=False)
    parent_parser_ta.add_argument(
        "-ta",
        "--target",
        dest="target",
        help="target file",
        required=False,
    )
    parent_parser_tp = argparse.ArgumentParser(add_help=False)
    parent_parser_tp.add_argument(
        "-tp",
        "--temp",
        dest="temp",
        help="temporary directory",
        required=False,
    )
    parent_parser_of = argparse.ArgumentParser(add_help=False)
    parent_parser_of.add_argument(
        "-of",
        "--outfile",
        dest="outfile",
        help="output file",
        required=False,
    )
    parent_parser_od = argparse.ArgumentParser(add_help=False)
    parent_parser_od.add_argument(
        "-od",
        "--outdir",
        dest="outdir",
        help="output directory",
        required=False,
    )
    parent_parser_cpu = argparse.ArgumentParser(add_help=False)
    parent_parser_cpu.add_argument(
        "-c",
        "--cpu",
        type=int,
        dest="cpu",
        help="number of cpu",
        required=False,
    )
    parent_parser_j = argparse.ArgumentParser(add_help=False)
    parent_parser_j.add_argument(
        "-j",
        "--json",
        dest="json",
        help="directory of json result files",
        required=True,
    )
    subparsers = parser.add_subparsers(
        title='subcommands',
        description='valid subcommands:',
        dest="cmd")

    b_concat_parser = subparsers.add_parser(
        "bconcat",
        help="Concat all sbml file of a directory into a single lp file for ASP.",
        parents=[
            parent_parser_b, parent_parser_od, parent_parser_gt, parent_parser_of, parent_parser_cpu
        ],
        description=
        """
        TODO
        """,
        usage="""
        **1** from directory of all bacterias SBML files \n
        powerfun bconcat -b symbiont_directory -gt genometax.tsv  [--output outputfile.json] [--cpu numcpu] \n 
        """
    )

    reaction_analysis_parser = subparsers.add_parser(
      "reaction",
      help="",
      parents=[
          parent_parser_b, parent_parser_s, parent_parser_p, 
          parent_parser_gt, parent_parser_ta, parent_parser_tp, 
          parent_parser_of, parent_parser_od, parent_parser_cpu
      ],
      description=
      """
      TODO
      """,
      usage="""
      powerfun reaction -b symbiont_directory -s seeds.sbml -p powergraph.bbl -gt genometax.tsv 
      [-ta targets.tsv] [-tp temporary_directory] [-of outputfile.json] [-od outputdir] [--cpu numcpu]
      """
    )

    reaction_analysis_parser = subparsers.add_parser(
      "metabolite",
      help="",
      parents=[
          parent_parser_b, parent_parser_s, parent_parser_p, 
          parent_parser_gt, parent_parser_ta, parent_parser_tp, 
          parent_parser_of, parent_parser_od, parent_parser_cpu
      ],
      description=
      """
      TODO
      """,
      usage="""
      powerfun metabolite -b symbiont_directory -s seeds.sbml -p powergraph.bbl -gt genometax.tsv 
      [-ta targets.tsv] [-tp temporary_directory] [-of outputfile.json] [-od outputdir] [--cpu numcpu]
      """
    )

    reaction_analysis_parser = subparsers.add_parser(
      "metareact",
      help="",
      parents=[
          parent_parser_b, parent_parser_s, parent_parser_p, 
          parent_parser_gt, parent_parser_ta, parent_parser_tp, 
          parent_parser_of, parent_parser_od, parent_parser_cpu
      ],
      description=
      """
      TODO
      """,
      usage="""
      powerfun metareact -b symbiont_directory -s seeds.sbml -p powergraph.bbl -gt genometax.tsv 
      [-ta targets.tsv] [-tp temporary_directory] [-of outputfile.json] [-od outputdir] [--cpu numcpu]
      """
    )

    all_analysis_parser = subparsers.add_parser(
      "analysis",
      help="",
      parents=[
          parent_parser_b, parent_parser_s, parent_parser_p, 
          parent_parser_gt, parent_parser_ta, parent_parser_tp, 
          parent_parser_of, parent_parser_od, parent_parser_cpu 
      ],
      description=
      """
      TODO
      """,
      usage="""
      powerfun analysis -b symbiont_directory -s seeds.sbml -p powergraph.bbl -gt genometax.tsv 
      [-ta targets.tsv] [-tp temporary_directory] [-of outputfile.json] [-od outputdir] [--cpu numcpu]
      """
    )

    convert_analysis = subparsers.add_parser(
      "convert_analysis",
      help="",
      parents=[
          parent_parser_j, parent_parser_od
      ],
      description=
      """
      TODO
      """,
      usage="""
      convert json results to tsv results\n
      powerfun convert_analysis -j input json directory [-od output directory json] 
      """
    )

    phylum_analysis_parser = subparsers.add_parser(
      "phylum",
      help="",
      parents=[
          parent_parser_b, parent_parser_s, parent_parser_p, 
          parent_parser_gt, parent_parser_ta, parent_parser_tp, 
          parent_parser_of, parent_parser_od, parent_parser_cpu
      ],
      description=
      """
      TODO
      """,
      usage="""
      powerfun phylum -b symbiont_directory -s seeds.sbml -p powergraph.bbl -gt genometax.tsv 
      [-ta targets.tsv] [-tp temporary_directory] [-of outputfile.json] [-od outputdir] [--cpu numcpu]
      """
    )

    test = subparsers.add_parser(
      "test",
      help="",
      parents=[
          parent_parser_j, parent_parser_od
      ],
      description=
      """
      TODO
      """,
      usage="""
      **1** convert json results to tsv results\n
      powerfun test -j input json directory [-od output directory json] 
      """
    )

    args = parser.parse_args()

    # If no argument print the help.
    if len(argv) == 1:
        parser.print_help()
        sys.exit(1)
    
    
    
    if args.cmd == "bconcat":
        logger.info("--- STARTING CONCATENATION ---")
        path = pf.create_concat_bact(args.bactsymbionts, args.genometax, args.outdir, args.outfile, args.cpu, True)
        logger.info("--- The concatenated lp file is at :" + path + " ---")

    if args.cmd == "reaction":
        logger.info("--- REACTION ANALYSIS ---")
        dirname = os.path.basename(os.path.dirname(args.bactsymbionts))
        all_bact_file_name = dirname + "_all_bact_instances" + commons.ASP_FILE_EXTENSION
        path_all_bact_file = pf.create_concat_bact(args.bactsymbionts, args.genometax, args.temp, all_bact_file_name, args.cpu)

        logger.info("--- Running ---")
        path = pf.run_analysis("reaction", args.seeds, args.bactsymbionts, 
                                args.powergraph, args.genometax, path_all_bact_file, args.target,
                                args.temp, args.outdir, args.outfile)
        logger.info("--- END ---")

    if args.cmd == "metabolite":
        logger.info("--- METABOLITE ANALYSIS ---")
        dirname = os.path.basename(os.path.dirname(args.bactsymbionts))
        all_bact_file_name = dirname + "_all_bact_instances" + commons.ASP_FILE_EXTENSION
        path_all_bact_file = pf.create_concat_bact(args.bactsymbionts, args.genometax, args.temp, all_bact_file_name, args.cpu)

        logger.info("--- Running ---")
        path = pf.run_analysis("metabolite", args.seeds, args.bactsymbionts, 
                                args.powergraph, args.genometax, path_all_bact_file, args.target,
                                args.temp, args.outdir, args.outfile)
        logger.info("--- END ---")

    if args.cmd == "metareact":
        logger.info("--- METABOLITE AND REACTION ANALYSIS ---")
        dirname = os.path.basename(os.path.dirname(args.bactsymbionts))
        all_bact_file_name = dirname + "_all_bact_instances" + commons.ASP_FILE_EXTENSION
        path_all_bact_file = pf.create_concat_bact(args.bactsymbionts, args.genometax, args.temp, all_bact_file_name, args.cpu)

        logger.info("--- Running ---")
        path = pf.run_analysis("metareact", args.seeds, args.bactsymbionts, 
                                args.powergraph, args.genometax, path_all_bact_file, args.target,
                                args.temp, args.outdir, args.outfile)
        logger.info("--- END ---")

    if args.cmd == "analysis":
        logger.info("--- ALL ANALYSIS ---")
        dirname = os.path.basename(os.path.dirname(args.bactsymbionts))
        all_bact_file_name = dirname + "_all_bact_instances" + commons.ASP_FILE_EXTENSION
        path_all_bact_file = pf.create_concat_bact(args.bactsymbionts, args.genometax, args.temp, all_bact_file_name, args.cpu)

        logger.info("--- Running ---")
        path = pf.run_analysis("analysis", args.seeds, args.bactsymbionts, 
                                args.powergraph, args.genometax, path_all_bact_file, args.target,
                                args.temp, args.outdir, args.outfile)
        logger.info("--- END ---")
    
    if args.cmd == "convert_analysis":
        path = pf.files.convert_results_to_tsv(args.json, args.outdir)
        logger.info("--- The tsv files are at :" + path + " ---")

    if args.cmd == "phylum":
        logger.info("--- METABOLITE ANALYSIS ---")
        dirname = os.path.basename(os.path.dirname(args.bactsymbionts))
        all_bact_file_name = dirname + "_all_bact_instances" + commons.ASP_FILE_EXTENSION
        path_all_bact_file = pf.create_concat_bact(args.bactsymbionts, args.genometax, args.temp, all_bact_file_name, args.cpu)

        logger.info("--- Running ---")
        path = pf.run_analysis("phylum", args.seeds, args.bactsymbionts, 
                                args.powergraph, args.genometax, path_all_bact_file, args.target,
                                args.temp, args.outdir, args.outfile)
        logger.info("--- END ---")

    if args.cmd == "test":
        path = pf.files.convert_results_to_tsv(args.json, args.outdir)
        logger.info("--- The tsv files are at :" + path + " ---")

    logger.info("--- Total runtime %.2f seconds --- \n" % (time.time() - start_time))


