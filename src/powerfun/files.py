import logging
import sys
from powerfun import utils, commons
from miscoto import utils as m_utils #, sbml, commons, query

logger = logging.getLogger(__name__)

def write_concat_sbml_asp_file(sbml_dir, gut_genome_file, tmp_species_dir, cpu):
    """
    """
    utils.convert_concat_sbml_asp_symbionts_multiprocess(sbml_dir, gut_genome_file, tmp_species_dir, cpu)


def write_sbml_asp_file(sbml_file, out_dir, name=None):
    """
    """
    ext = "." + commons.extension(sbml_file)
    if(not ext or ext != commons.SBML_FILE_EXTENSION):
        logger.info(sbml_file + ' is not an sbml file.')
        sys.exit(1)
    if not name:
        name = commons.basename(sbml_file) + commons.ASP_FILE_EXTENSION 
    model = utils.convert_sbml_asp_file("seeds", sbml_file)
    # Write into a file
    path = out_dir + "/" + name
    m_utils.to_file(model, path)
    return path


def write_bbl_asp_file(bbl_file, gut_genome_file, out_dir, name=None):
    """
    """
    ext = "." + commons.extension(bbl_file)
    if(not ext or ext != commons.BBL_FILE_EXTENSION):
        logger.info(bbl_file + ' is not a bbl file.')
        sys.exit(1)
    if not name:
        name = commons.basename(bbl_file) + commons.ASP_FILE_EXTENSION
    path = out_dir + "/" + name
    model = utils.convert_bbl_asp(bbl_file, gut_genome_file)
    m_utils.to_file(model, path)
    return path

def write_tsv_asp_target_file(tsv_file, out_dir, name=None):
    ext = "." + commons.extension(tsv_file)
    if(not ext or ext != commons.TSV_FILE_EXTENSION):
        logger.info(tsv_file + ' is not a tsv file.')
        sys.exit(1)
    if not name:
        name = commons.basename(tsv_file) + commons.ASP_FILE_EXTENSION
    path = out_dir + "/" + name
    model = utils.convert_tsv_asp_target_file(tsv_file)
    m_utils.to_file(model, path)
    return path

def convert_results_to_tsv(json_file, out_dir, name=None):
    ext = "." + commons.extension(json_file)
    if(not ext or ext != commons.JSON_FILE_EXTENSION):
        logger.info(json_file + ' is not a json file.')
        sys.exit(1)
    if not name:
        name = commons.basename(json_file) + commons.TSV_FILE_EXTENSION
    path = out_dir + "/" + name
    df = utils.convert_json_to_tsv(json_file)
    df.to_csv(path, sep="\t")
    return path