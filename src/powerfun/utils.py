from curses import meta
import sys 
import os
import logging
import pandas as pd
import json

from multiprocessing import Pool
from functools import partial
from bubbletools import BubbleTree
from clyngor.as_pyasp import TermSet, Atom
from miscoto import sbml, utils as m_utils
from padmet.utils.sbmlPlugin import convert_to_coded_id as padmet_convert_to_coded_id 
from padmet.utils.sbmlPlugin import convert_from_coded_id as padmet_decode_id
from powerfun import commons

import xml.etree.ElementTree as etree

logger = logging.getLogger(__name__)


def convert_bbl_tree(bbl_file):
    try:
        tree = BubbleTree.from_bubble_file(bbl_file)
    except FileNotFoundError:
        logger.critical('PowerGraph file not found:' + bbl_file)
        sys.exit(1)
    return tree


def convert_bbl_asp(bbl_file, gut_genome_file):
    """
    """
    all_atoms = set()
    tree = convert_bbl_tree(bbl_file)
    full_connected_pn = []
    pn_type = ""
    try:
        # Load csv
        gut_genome_df = pd.read_csv(gut_genome_file, sep=";", index_col="ink_name")
    except FileNotFoundError:
        logger.critical('Conversion file not found:' + gut_genome_file)
        sys.exit(1)

    for edge in tree.edges.items():
        if edge[0] in edge[1]:
            full_connected_pn.append(edge[0])
    powernode_inclusion = tree.inclusions
    for item in powernode_inclusion.items():
        if(not item[1]):
            all_atoms.add(Atom('node', 
                                ["\""+item[0]+"\""]))
        else:
            if (item[0] in full_connected_pn):
                    pn_type="independant"
            else:
                    pn_type="group"
            all_atoms.add(Atom('powernode', ["\""+item[0]+"\"","\""+pn_type+"\""]))
            nodes_included = list(item[1])
            for node in nodes_included:
                if (node in gut_genome_df.index):
                    node_id=gut_genome_df["species"].loc[node]
                else:
                    node_id=node
                all_atoms.add(Atom('included', ["\""+node_id+"\"", "\""+item[0]+"\""]))

    lpfacts = TermSet(all_atoms)
    return lpfacts


def convert_sbml_asp_file(type, sbml_file, name=None):
    """
    """
    try:
        if type =="seeds":
            model  = sbml.readSBMLspecies_clyngor(sbml_file, "seed")
        else:
            if not name:
                name = commons.basename(sbml_file)
            model  = readSBMLnetwork_symbionts_clyngor(sbml_file, name)
            model.add(Atom('bacteria', ["\"" + name + "\""]))
    except:
        logger.critical('Conversion file not found:' + sbml_file)
        sys.exit(1)
    return model



def run_convert_concat_sbml_asp_symbionts_multiprocess(species_multiprocess_data, tmp_species_dir, gut_genome_file):
    """
    Args:
        
    Returns:
        
    """
    try:
        # Load csv
        gut_genome_df = pd.read_csv(gut_genome_file, sep=";", index_col="species")
    except FileNotFoundError:
        logger.critical('Conversion file not found:' + gut_genome_file)
        sys.exit(1)
        
    species_sbml_file = species_multiprocess_data[0]
    name = commons.basename(species_sbml_file)
    one_bact_model  = readSBMLnetwork_symbionts_clyngor(species_sbml_file, name)
    one_bact_model.add(Atom('bacteria', ["\"" + name + "\""]))
    one_bact_model.add(Atom('phyl', 
                        ["\""+name+"\"", 
                        "\""+gut_genome_df["ink_name"].loc[name]+"\"",
                        "\""+gut_genome_df["phylum"].loc[name]+"\"",
                        "\""+gut_genome_df["class"].loc[name]+"\"",
                        "\""+gut_genome_df["order"].loc[name]+"\"",
                        "\""+gut_genome_df["family"].loc[name]+"\"",
                        "\""+gut_genome_df["genus"].loc[name]+"\""]))
    lp_species_path = os.path.join(tmp_species_dir, name + ".lp")
    m_utils.to_file(one_bact_model,lp_species_path)
    #return one_bact_model


def convert_concat_sbml_asp_symbionts_multiprocess(sbml_dir, gut_genome_file, tmp_species_dir, cpu):
    """
    Args:
        
    Returns:
        
    """
    sbml_to_lp_pool = Pool(processes=cpu) 

    multiprocess_data = []

    for species in os.listdir(sbml_dir):
        sbml_species_path = os.path.join(sbml_dir, species)
        multiprocess_data.append([sbml_species_path])
    sbml_to_lp_pool.map(partial(run_convert_concat_sbml_asp_symbionts_multiprocess,tmp_species_dir=tmp_species_dir, gut_genome_file= gut_genome_file), \
                        multiprocess_data)
    
    sbml_to_lp_pool.close()
    sbml_to_lp_pool.join()




def convert_tsv_asp_target_file(tsv_file):
    all_atoms = set()
    targets_df = pd.read_csv(tsv_file, sep=';', header=0)
    for target in targets_df["ID"]:
        target_converted = padmet_convert_to_coded_id(target,"M","c")
        all_atoms.add(Atom('target', ["\""+target_converted+"\""]))
    lpfacts = TermSet(all_atoms)
    return lpfacts


def readSBMLnetwork_symbionts_clyngor(filename, name) :
    """Read a SBML metabolic network
    
    Args:
        filename (str): SBML file
        name (str): suffix to identify the type of network
    
    Returns:
        TermSet: metabolic model
    """
    all_atoms = set()
    tree = etree.parse(filename)
    f_sbml = tree.getroot()
    model = sbml.get_model(f_sbml)

    listOfReactions = sbml.get_listOfReactions(model)
    if listOfReactions is None:
        logger.critical('No reaction in SBML '+filename)
        sys.exit(1)
    for e in listOfReactions:
        if e.tag[0] == "{":
            uri, tag = e.tag[1:].split("}")
        else:
            tag = e.tag
        if tag == "reaction":
            reactionId = e.attrib.get("id")
            all_atoms.add(Atom('reaction', ["\""+reactionId+"\"", "\""+name+"\""])) 
            if(e.attrib.get("reversible")=="true"):
                all_atoms.add(Atom('reaction', ["\""+reactionId+"_rev\"", "\""+name+"\""]))

            listOfReactants = sbml.get_listOfReactants(e)
            if listOfReactants != None:
                for r in listOfReactants:
                    all_atoms.add(Atom('reactant', ["\""+r.attrib.get("species").replace('"','')+"\"", "\""+reactionId+"\"", "\""+name+"\""])) 
                    if(e.attrib.get("reversible")=="true"):
                        all_atoms.add(Atom('product', ["\""+r.attrib.get("species").replace('"','')+"\"", "\""+reactionId+"_rev\"", "\""+name+"\""]))

            listOfProducts = sbml.get_listOfProducts(e)
            if listOfProducts != None:
                for p in listOfProducts:
                    all_atoms.add(Atom('product', ["\""+p.attrib.get("species").replace('"','')+"\"", "\""+reactionId+"\"", "\""+name+"\""]))
                    if(e.attrib.get("reversible")=="true"):
                        all_atoms.add(Atom('reactant', ["\""+p.attrib.get("species").replace('"','')+"\"", "\""+reactionId+"_rev\"", "\""+name+"\""]))
                     
    listofspecies = sbml.get_listOfSpecies(model)
    for e in listofspecies:
        if e.tag[0] == "{":
            uri, tag = e.tag[1:].split("}")
        else: tag = e.tag
        if tag == "species":
            try:
                speciesId = e.attrib.get("id").replace('"','')
            except AttributeError:
                sys.exit("Empty ID field for species in the following SBML: " + filename)
            try:
                speciesNm = e.attrib.get("name").replace('"','')
            except AttributeError:
                sys.exit("Empty name field for species in the following SBML: " + filename)
            try:
                compartment = e.attrib.get("compartment")
            except AttributeError:
                sys.exit("Empty compartment field for species in the following SBML: " + filename)
            all_atoms.add(Atom('species', ["\""+speciesId+"\"", "\""+speciesNm+"\"", "\""+compartment+"\"", "\""+name+"\""]))

    lpfacts = TermSet(all_atoms)

    return lpfacts

def convert_json_to_tsv(json_file):
    with open(json_file, "r") as f:
        models = json.load(f)
        num_model = len(models)
        rows = []
        for it in range(num_model):
            meta_done=[]
            #TODO case for different type json
            model = models["model_" + str(it)]
            powernode = model[1]
            reaction_only_all_selected_activ = model[3]
            producible_only_all_selected = model[7]
            transfered_prod_only_all_selected = model[11]
            produce_target_selected = model[13]
            produce_target_all_selected = model[15]
            produce_target_only_all_selected = model[17]
            mr_activ_only_all_selected = model[21]

            for reaction in reaction_only_all_selected_activ:
                reaction_decoded =  padmet_decode_id(reaction, compart_in_id=True)[0]
                is_metareact = False
                if reaction in mr_activ_only_all_selected:
                    is_metareact = True
                    for metareact_metabolite in mr_activ_only_all_selected[reaction]:
                        meta_done.append(metareact_metabolite)
                        meta_status = get_meta_status(metareact_metabolite,
                                        producible_only_all_selected,
                                        transfered_prod_only_all_selected,
                                        produce_target_selected,
                                        produce_target_all_selected,
                                        produce_target_only_all_selected)
                        rows.append([powernode, reaction_decoded, str(is_metareact)] + meta_status)
                else:
                    rows.append([powernode, reaction_decoded, str(is_metareact), "", "", "", "", "", "", ""])
                    

            for metabolite in producible_only_all_selected:
                if metabolite not in meta_done:
                    meta_done.append(metabolite)
                    meta_status = get_meta_status(metabolite,
                                        producible_only_all_selected,
                                        transfered_prod_only_all_selected,
                                        produce_target_selected,
                                        produce_target_all_selected,
                                        produce_target_only_all_selected)
                    rows.append([powernode, "", ""] + meta_status)

            for target in produce_target_selected:
                if target not in meta_done:
                    meta_done.append(target)
                    meta_status = get_meta_status(target,
                                        producible_only_all_selected,
                                        transfered_prod_only_all_selected,
                                        produce_target_selected,
                                        produce_target_all_selected,
                                        produce_target_only_all_selected)
                    
                    rows.append([powernode, "", ""] + meta_status)

    df_result = pd.DataFrame(data = rows, columns=["PN",
                                "activ reaction only all selected",
                                "is active metareact ony all selected",
                                "Meta producible selected having properties",
                                "is only all selected",
                                "is transfered only all selected",
                                "To powernode",
                                "is target from selected",
                                "is target all selected",
                                "is target only all selected"])
    return df_result

#def convert_json_to_tsv_long(json_file):
#    with open(json_file, "r") as f:
#        models = json.load(f)
#        num_model = len(models)
#        rows = []
#        for it in range(num_model):
#            meta_done=[]
#            #TODO case for different type json
#            model = models["model_" + str(it)]
#            powernode = model[1]
#            reaction_only_all_selected_outside_activ = model[3]
#            reaction_only_all_selected_activ = model[7]
#            producible_only_all_selected = model[11]
#            transfered_prod_only_all_selected = model[15]
#            produce_target_selected = model[17]
#            produce_target_all_selected = model[19]
#            produce_target_only_all_selected = model[21]
#            mr_activ_only_all_selected_outside = model[25]
#            mr_activ_only_all_selected = model[29]
#
#            for reaction in reaction_only_all_selected_outside_activ:
#                is_exclusive_selected = False
#                is_metareact = False
#                is_metareact_exclusive_selected = False
#                if reaction in reaction_only_all_selected_activ:
#                    is_exclusive_selected = True
#                if reaction in mr_activ_only_all_selected_outside:
#                    is_metareact = True
#                    if reaction in mr_activ_only_all_selected:
#                        is_metareact_exclusive_selected = True
#                    for metareact_metabolite in mr_activ_only_all_selected_outside[reaction]:
#                        meta_done.append(metareact_metabolite)
#                        meta_status = get_meta_status(metareact_metabolite,
#                                        producible_only_all_selected,
#                                        transfered_prod_only_all_selected,
#                                        produce_target_selected,
#                                        produce_target_all_selected,
#                                        produce_target_only_all_selected)
#                        rows.append([powernode, reaction, str(is_exclusive_selected), str(is_metareact), 
#                                    str(is_metareact_exclusive_selected)] + meta_status)
#                else:
#                    rows.append([powernode, reaction, str(is_exclusive_selected), str(is_metareact), 
#                    str(is_metareact_exclusive_selected), "", "", "", "", "", "", ""])
#                   
#
#            for metabolite in producible_only_all_selected:
#                if metabolite not in meta_done:
#                    meta_done.append(metabolite)
#                    meta_status = get_meta_status(metabolite,
#                                        producible_only_all_selected,
#                                        transfered_prod_only_all_selected,
#                                        produce_target_selected,
#                                        produce_target_all_selected,
#                                        produce_target_only_all_selected)
#                    rows.append([powernode, "", "", "", ""] + meta_status)
#
#            for target in produce_target_selected:
#                if target not in meta_done:
#                    meta_done.append(target)
#                    meta_status = get_meta_status(target,
#                                        producible_only_all_selected,
#                                        transfered_prod_only_all_selected,
#                                        produce_target_selected,
#                                        produce_target_all_selected,
#                                        produce_target_only_all_selected)
#                    
#                    rows.append([powernode, "", "", "", ""] + meta_status)
#
#    df_result = pd.DataFrame(data = rows, columns=["PN",
#                                "activ reaction only all selected and outside",
#                                "is activ reaction exclusively only all selected",
#                                "is active metareact only all selected and outside",
#                                "is active metareact exclusive only all selected",
#                                "Meta producible selected having properties",
#                                "is only all selected",
#                                "is transfered only all selected",
#                                "To powernode",
#                                "is target from selected",
#                                "is target all selected",
#                                "is target only all selected"])
#    return df_result


def get_meta_status(metabolite, 
                    producible_only_all_selected,
                    transfered_prod_only_all_selected,
                    produce_target_selected,
                    produce_target_all_selected,
                    produce_target_only_all_selected):
    metabolite_decoded = padmet_decode_id(metabolite, compart_in_id=True)[0]
    is_only_all_selected = False
    is_transf_only_all_selected = False
    transf_to_powernodes = ""
    is_target = False
    is_target_all_selected = False
    is_target_only_selected = False
    if metabolite in producible_only_all_selected:
        is_only_all_selected = True
    if metabolite in transfered_prod_only_all_selected:
        is_transf_only_all_selected = True
        for pn in transfered_prod_only_all_selected[metabolite]:
            transf_to_powernodes = transf_to_powernodes + pn + "; "
    if metabolite in produce_target_selected:
        is_target = True
    if metabolite in produce_target_all_selected:
        is_target_all_selected = True
    if metabolite in produce_target_only_all_selected:
        is_target_only_selected = True
    meta_status = [metabolite_decoded,
                    str(is_only_all_selected),
                    str(is_transf_only_all_selected),
                    transf_to_powernodes, 
                    str(is_target), 
                    str(is_target_all_selected), 
                    str(is_target_only_selected)]
    return meta_status