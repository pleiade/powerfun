"""
Definition of numerous constants, for paths, names, arguments for ASP solvers.
"""

import os
import shutil

# Root
ROOT = os.path.dirname(__file__)

# Constants
ASP_FILE_EXTENSION = '.lp'
SBML_FILE_EXTENSION = '.sbml'
BBL_FILE_EXTENSION = '.bbl'
JSON_FILE_EXTENSION = '.json'
TSV_FILE_EXTENSION = '.tsv'

# Directories (starting from here)
DIR_SOURCES     = ''  # sources are inside the package
DIR_ASP_SOURCES = 'encodings'
DIR_DATA        = os.path.join(*[ROOT , '..', 'data'])  # datas are inside the package

DIR_POWERFUN    = os.path.expanduser('~/powerfun')
DIR_RESULT      = os.path.join(*[DIR_POWERFUN, 'result'])
DIR_TEMP        = os.path.join(*[DIR_POWERFUN ,'temp'])

# ASP SOURCES
def __asp_file(name):
    "path to given asp source file name"
    return os.path.join(*[ROOT, DIR_ASP_SOURCES, name + ASP_FILE_EXTENSION])

# Routine
ASP_SRC_SELECTION = __asp_file('selection_analysis')
ASP_SRC_REACTIONS = __asp_file('reaction_analysis')
ASP_SRC_METABOLITES = __asp_file('metabolite_analysis')
ASP_SRC_META_REACT = __asp_file('mr_analysis')
ASP_SRC_PHYLUM = __asp_file('phylum_analysis')



def basename(filepath):
    """Return the basename of given filepath.
    >>> basename('~/an/interesting/file.lp')
    'file'
    """
    return os.path.splitext(os.path.basename(filepath))[0]

def extension(filepath):
    """Return the extension of given filepath.
    >>> extension('~/an/interesting/file.lp')
    'lp'
    >>> extension('nothing')
    ''
    >>> extension('nothing.important')
    'important'
    """
    return os.path.splitext(os.path.basename(filepath))[1][1:]

def clean_up_file(tmp_dir_path, tmp_file_name) :
    file_path = os.path.join(*[tmp_dir_path ,tmp_file_name])
    if os.path.isfile(file_path): os.remove(file_path)

def clean_up_dir(tmp_dir_path) :
    if os.path.isdir(tmp_dir_path): shutil.rmtree(tmp_dir_path)