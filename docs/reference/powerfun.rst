powerfun
========

.. testsetup::

    from powerfun import *

.. automodule:: powerfun
    :members:
