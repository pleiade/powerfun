graph [
  node [
    id 0
    label "Firm84"
  ]
  node [
    id 1
    label "Firm739"
  ]
  node [
    id 2
    label "Firm529"
  ]
  node [
    id 3
    label "Prot29"
  ]
  node [
    id 4
    label "Prot17"
  ]
  node [
    id 5
    label "Prot22"
  ]
  node [
    id 6
    label "Prot13"
  ]
  node [
    id 7
    label "Prot2"
  ]
  node [
    id 8
    label "Prot11"
  ]
  node [
    id 9
    label "Prot4"
  ]
  node [
    id 10
    label "Prot30"
  ]
  node [
    id 11
    label "Prot36"
  ]
  node [
    id 12
    label "Prot16"
  ]
  node [
    id 13
    label "Firm504"
  ]
  node [
    id 14
    label "Firm403"
  ]
  node [
    id 15
    label "Firm439"
  ]
  node [
    id 16
    label "Firm554"
  ]
  node [
    id 17
    label "Firm249"
  ]
  node [
    id 18
    label "Firm443"
  ]
  node [
    id 19
    label "Firm551"
  ]
  node [
    id 20
    label "Firm520"
  ]
  node [
    id 21
    label "Firm449"
  ]
  node [
    id 22
    label "Firm761"
  ]
  node [
    id 23
    label "Firm112"
  ]
  node [
    id 24
    label "Firm735"
  ]
  node [
    id 25
    label "Firm616"
  ]
  node [
    id 26
    label "Firm672"
  ]
  node [
    id 27
    label "Firm506"
  ]
  node [
    id 28
    label "Firm115"
  ]
  node [
    id 29
    label "Firm540"
  ]
  node [
    id 30
    label "Firm405"
  ]
  node [
    id 31
    label "Firm15"
  ]
  node [
    id 32
    label "Firm238"
  ]
  node [
    id 33
    label "Firm50"
  ]
  node [
    id 34
    label "Firm446"
  ]
  node [
    id 35
    label "Firm680"
  ]
  node [
    id 36
    label "Firm715"
  ]
  node [
    id 37
    label "Firm14"
  ]
  node [
    id 38
    label "Firm661"
  ]
  node [
    id 39
    label "Firm690"
  ]
  node [
    id 40
    label "Firm338"
  ]
  node [
    id 41
    label "Firm280"
  ]
  node [
    id 42
    label "Firm283"
  ]
  node [
    id 43
    label "Firm765"
  ]
  node [
    id 44
    label "Firm622"
  ]
  node [
    id 45
    label "Firm65"
  ]
  node [
    id 46
    label "Prot9"
  ]
  node [
    id 47
    label "Firm565"
  ]
  node [
    id 48
    label "Firm469"
  ]
  node [
    id 49
    label "Firm246"
  ]
  node [
    id 50
    label "Firm220"
  ]
  node [
    id 51
    label "Firm484"
  ]
  node [
    id 52
    label "Firm652"
  ]
  node [
    id 53
    label "Firm133"
  ]
  node [
    id 54
    label "Firm696"
  ]
  node [
    id 55
    label "Firm126"
  ]
  node [
    id 56
    label "Firm55"
  ]
  node [
    id 57
    label "Firm511"
  ]
  node [
    id 58
    label "Prot20"
  ]
  node [
    id 59
    label "Prot10"
  ]
  node [
    id 60
    label "Prot31"
  ]
  node [
    id 61
    label "Prot1"
  ]
  node [
    id 62
    label "Firm512"
  ]
  node [
    id 63
    label "Firm334"
  ]
  node [
    id 64
    label "Firm106"
  ]
  node [
    id 65
    label "Firm96"
  ]
  node [
    id 66
    label "Firm594"
  ]
  node [
    id 67
    label "Prot7"
  ]
  node [
    id 68
    label "Prot6"
  ]
  node [
    id 69
    label "Prot15"
  ]
  node [
    id 70
    label "Prot28"
  ]
  node [
    id 71
    label "Prot35"
  ]
  edge [
    source 0
    target 1
    weight 19
  ]
  edge [
    source 0
    target 2
    weight 19
  ]
  edge [
    source 0
    target 3
    weight 19
  ]
  edge [
    source 0
    target 4
    weight 1
  ]
  edge [
    source 0
    target 5
    weight 1
  ]
  edge [
    source 0
    target 6
    weight 1
  ]
  edge [
    source 0
    target 7
    weight 1
  ]
  edge [
    source 0
    target 8
    weight 1
  ]
  edge [
    source 0
    target 9
    weight 1
  ]
  edge [
    source 0
    target 10
    weight 1
  ]
  edge [
    source 0
    target 11
    weight 1
  ]
  edge [
    source 0
    target 12
    weight 1
  ]
  edge [
    source 0
    target 58
    weight 1
  ]
  edge [
    source 0
    target 59
    weight 1
  ]
  edge [
    source 0
    target 60
    weight 1
  ]
  edge [
    source 0
    target 61
    weight 1
  ]
  edge [
    source 0
    target 46
    weight 1
  ]
  edge [
    source 0
    target 67
    weight 1
  ]
  edge [
    source 0
    target 68
    weight 1
  ]
  edge [
    source 0
    target 69
    weight 1
  ]
  edge [
    source 0
    target 70
    weight 1
  ]
  edge [
    source 0
    target 71
    weight 1
  ]
  edge [
    source 1
    target 2
    weight 950
  ]
  edge [
    source 1
    target 3
    weight 950
  ]
  edge [
    source 1
    target 4
    weight 50
  ]
  edge [
    source 1
    target 5
    weight 50
  ]
  edge [
    source 1
    target 6
    weight 50
  ]
  edge [
    source 1
    target 7
    weight 50
  ]
  edge [
    source 1
    target 8
    weight 50
  ]
  edge [
    source 1
    target 9
    weight 50
  ]
  edge [
    source 1
    target 10
    weight 50
  ]
  edge [
    source 1
    target 11
    weight 50
  ]
  edge [
    source 1
    target 12
    weight 50
  ]
  edge [
    source 1
    target 13
    weight 19
  ]
  edge [
    source 1
    target 14
    weight 19
  ]
  edge [
    source 1
    target 15
    weight 19
  ]
  edge [
    source 1
    target 16
    weight 19
  ]
  edge [
    source 1
    target 17
    weight 19
  ]
  edge [
    source 1
    target 18
    weight 19
  ]
  edge [
    source 1
    target 19
    weight 19
  ]
  edge [
    source 1
    target 20
    weight 19
  ]
  edge [
    source 1
    target 21
    weight 19
  ]
  edge [
    source 1
    target 22
    weight 19
  ]
  edge [
    source 1
    target 23
    weight 19
  ]
  edge [
    source 1
    target 24
    weight 19
  ]
  edge [
    source 1
    target 25
    weight 19
  ]
  edge [
    source 1
    target 26
    weight 19
  ]
  edge [
    source 1
    target 27
    weight 19
  ]
  edge [
    source 1
    target 28
    weight 19
  ]
  edge [
    source 1
    target 29
    weight 19
  ]
  edge [
    source 1
    target 30
    weight 19
  ]
  edge [
    source 1
    target 31
    weight 19
  ]
  edge [
    source 1
    target 32
    weight 19
  ]
  edge [
    source 1
    target 33
    weight 19
  ]
  edge [
    source 1
    target 34
    weight 19
  ]
  edge [
    source 1
    target 35
    weight 19
  ]
  edge [
    source 1
    target 36
    weight 19
  ]
  edge [
    source 1
    target 37
    weight 19
  ]
  edge [
    source 1
    target 38
    weight 19
  ]
  edge [
    source 1
    target 39
    weight 19
  ]
  edge [
    source 1
    target 40
    weight 19
  ]
  edge [
    source 1
    target 41
    weight 19
  ]
  edge [
    source 1
    target 42
    weight 19
  ]
  edge [
    source 1
    target 43
    weight 19
  ]
  edge [
    source 1
    target 44
    weight 19
  ]
  edge [
    source 1
    target 45
    weight 19
  ]
  edge [
    source 1
    target 46
    weight 50
  ]
  edge [
    source 1
    target 47
    weight 19
  ]
  edge [
    source 1
    target 48
    weight 19
  ]
  edge [
    source 1
    target 49
    weight 19
  ]
  edge [
    source 1
    target 50
    weight 19
  ]
  edge [
    source 1
    target 51
    weight 19
  ]
  edge [
    source 1
    target 52
    weight 19
  ]
  edge [
    source 1
    target 53
    weight 19
  ]
  edge [
    source 1
    target 54
    weight 19
  ]
  edge [
    source 1
    target 55
    weight 19
  ]
  edge [
    source 1
    target 56
    weight 19
  ]
  edge [
    source 1
    target 57
    weight 19
  ]
  edge [
    source 1
    target 58
    weight 50
  ]
  edge [
    source 1
    target 59
    weight 50
  ]
  edge [
    source 1
    target 60
    weight 50
  ]
  edge [
    source 1
    target 61
    weight 50
  ]
  edge [
    source 1
    target 62
    weight 19
  ]
  edge [
    source 1
    target 63
    weight 19
  ]
  edge [
    source 1
    target 64
    weight 19
  ]
  edge [
    source 1
    target 65
    weight 19
  ]
  edge [
    source 1
    target 66
    weight 19
  ]
  edge [
    source 1
    target 67
    weight 50
  ]
  edge [
    source 1
    target 68
    weight 50
  ]
  edge [
    source 1
    target 69
    weight 50
  ]
  edge [
    source 1
    target 70
    weight 50
  ]
  edge [
    source 1
    target 71
    weight 50
  ]
  edge [
    source 2
    target 3
    weight 950
  ]
  edge [
    source 2
    target 4
    weight 50
  ]
  edge [
    source 2
    target 5
    weight 50
  ]
  edge [
    source 2
    target 6
    weight 50
  ]
  edge [
    source 2
    target 7
    weight 50
  ]
  edge [
    source 2
    target 8
    weight 50
  ]
  edge [
    source 2
    target 9
    weight 50
  ]
  edge [
    source 2
    target 10
    weight 50
  ]
  edge [
    source 2
    target 11
    weight 50
  ]
  edge [
    source 2
    target 12
    weight 50
  ]
  edge [
    source 2
    target 13
    weight 19
  ]
  edge [
    source 2
    target 14
    weight 19
  ]
  edge [
    source 2
    target 15
    weight 19
  ]
  edge [
    source 2
    target 16
    weight 19
  ]
  edge [
    source 2
    target 17
    weight 19
  ]
  edge [
    source 2
    target 18
    weight 19
  ]
  edge [
    source 2
    target 19
    weight 19
  ]
  edge [
    source 2
    target 20
    weight 19
  ]
  edge [
    source 2
    target 21
    weight 19
  ]
  edge [
    source 2
    target 22
    weight 19
  ]
  edge [
    source 2
    target 23
    weight 19
  ]
  edge [
    source 2
    target 24
    weight 19
  ]
  edge [
    source 2
    target 25
    weight 19
  ]
  edge [
    source 2
    target 26
    weight 19
  ]
  edge [
    source 2
    target 27
    weight 19
  ]
  edge [
    source 2
    target 28
    weight 19
  ]
  edge [
    source 2
    target 29
    weight 19
  ]
  edge [
    source 2
    target 30
    weight 19
  ]
  edge [
    source 2
    target 31
    weight 19
  ]
  edge [
    source 2
    target 32
    weight 19
  ]
  edge [
    source 2
    target 33
    weight 19
  ]
  edge [
    source 2
    target 34
    weight 19
  ]
  edge [
    source 2
    target 35
    weight 19
  ]
  edge [
    source 2
    target 36
    weight 19
  ]
  edge [
    source 2
    target 37
    weight 19
  ]
  edge [
    source 2
    target 38
    weight 19
  ]
  edge [
    source 2
    target 39
    weight 19
  ]
  edge [
    source 2
    target 40
    weight 19
  ]
  edge [
    source 2
    target 41
    weight 19
  ]
  edge [
    source 2
    target 42
    weight 19
  ]
  edge [
    source 2
    target 43
    weight 19
  ]
  edge [
    source 2
    target 44
    weight 19
  ]
  edge [
    source 2
    target 45
    weight 19
  ]
  edge [
    source 2
    target 46
    weight 50
  ]
  edge [
    source 2
    target 47
    weight 19
  ]
  edge [
    source 2
    target 48
    weight 19
  ]
  edge [
    source 2
    target 49
    weight 19
  ]
  edge [
    source 2
    target 50
    weight 19
  ]
  edge [
    source 2
    target 51
    weight 19
  ]
  edge [
    source 2
    target 52
    weight 19
  ]
  edge [
    source 2
    target 53
    weight 19
  ]
  edge [
    source 2
    target 54
    weight 19
  ]
  edge [
    source 2
    target 55
    weight 19
  ]
  edge [
    source 2
    target 56
    weight 19
  ]
  edge [
    source 2
    target 57
    weight 19
  ]
  edge [
    source 2
    target 58
    weight 50
  ]
  edge [
    source 2
    target 59
    weight 50
  ]
  edge [
    source 2
    target 60
    weight 50
  ]
  edge [
    source 2
    target 61
    weight 50
  ]
  edge [
    source 2
    target 62
    weight 19
  ]
  edge [
    source 2
    target 63
    weight 19
  ]
  edge [
    source 2
    target 64
    weight 19
  ]
  edge [
    source 2
    target 65
    weight 19
  ]
  edge [
    source 2
    target 66
    weight 19
  ]
  edge [
    source 2
    target 67
    weight 50
  ]
  edge [
    source 2
    target 68
    weight 50
  ]
  edge [
    source 2
    target 69
    weight 50
  ]
  edge [
    source 2
    target 70
    weight 50
  ]
  edge [
    source 2
    target 71
    weight 50
  ]
  edge [
    source 3
    target 4
    weight 50
  ]
  edge [
    source 3
    target 5
    weight 50
  ]
  edge [
    source 3
    target 6
    weight 50
  ]
  edge [
    source 3
    target 7
    weight 50
  ]
  edge [
    source 3
    target 8
    weight 50
  ]
  edge [
    source 3
    target 9
    weight 50
  ]
  edge [
    source 3
    target 10
    weight 50
  ]
  edge [
    source 3
    target 11
    weight 50
  ]
  edge [
    source 3
    target 12
    weight 50
  ]
  edge [
    source 3
    target 13
    weight 19
  ]
  edge [
    source 3
    target 14
    weight 19
  ]
  edge [
    source 3
    target 15
    weight 19
  ]
  edge [
    source 3
    target 16
    weight 19
  ]
  edge [
    source 3
    target 17
    weight 19
  ]
  edge [
    source 3
    target 18
    weight 19
  ]
  edge [
    source 3
    target 19
    weight 19
  ]
  edge [
    source 3
    target 20
    weight 19
  ]
  edge [
    source 3
    target 21
    weight 19
  ]
  edge [
    source 3
    target 22
    weight 19
  ]
  edge [
    source 3
    target 23
    weight 19
  ]
  edge [
    source 3
    target 24
    weight 19
  ]
  edge [
    source 3
    target 25
    weight 19
  ]
  edge [
    source 3
    target 26
    weight 19
  ]
  edge [
    source 3
    target 27
    weight 19
  ]
  edge [
    source 3
    target 28
    weight 19
  ]
  edge [
    source 3
    target 29
    weight 19
  ]
  edge [
    source 3
    target 30
    weight 19
  ]
  edge [
    source 3
    target 31
    weight 19
  ]
  edge [
    source 3
    target 32
    weight 19
  ]
  edge [
    source 3
    target 33
    weight 19
  ]
  edge [
    source 3
    target 34
    weight 19
  ]
  edge [
    source 3
    target 35
    weight 19
  ]
  edge [
    source 3
    target 36
    weight 19
  ]
  edge [
    source 3
    target 37
    weight 19
  ]
  edge [
    source 3
    target 38
    weight 19
  ]
  edge [
    source 3
    target 39
    weight 19
  ]
  edge [
    source 3
    target 40
    weight 19
  ]
  edge [
    source 3
    target 41
    weight 19
  ]
  edge [
    source 3
    target 42
    weight 19
  ]
  edge [
    source 3
    target 43
    weight 19
  ]
  edge [
    source 3
    target 44
    weight 19
  ]
  edge [
    source 3
    target 45
    weight 19
  ]
  edge [
    source 3
    target 46
    weight 50
  ]
  edge [
    source 3
    target 47
    weight 19
  ]
  edge [
    source 3
    target 48
    weight 19
  ]
  edge [
    source 3
    target 49
    weight 19
  ]
  edge [
    source 3
    target 50
    weight 19
  ]
  edge [
    source 3
    target 51
    weight 19
  ]
  edge [
    source 3
    target 52
    weight 19
  ]
  edge [
    source 3
    target 53
    weight 19
  ]
  edge [
    source 3
    target 54
    weight 19
  ]
  edge [
    source 3
    target 55
    weight 19
  ]
  edge [
    source 3
    target 56
    weight 19
  ]
  edge [
    source 3
    target 57
    weight 19
  ]
  edge [
    source 3
    target 58
    weight 50
  ]
  edge [
    source 3
    target 59
    weight 50
  ]
  edge [
    source 3
    target 60
    weight 50
  ]
  edge [
    source 3
    target 61
    weight 50
  ]
  edge [
    source 3
    target 62
    weight 19
  ]
  edge [
    source 3
    target 63
    weight 19
  ]
  edge [
    source 3
    target 64
    weight 19
  ]
  edge [
    source 3
    target 65
    weight 19
  ]
  edge [
    source 3
    target 66
    weight 19
  ]
  edge [
    source 3
    target 67
    weight 50
  ]
  edge [
    source 3
    target 68
    weight 50
  ]
  edge [
    source 3
    target 69
    weight 50
  ]
  edge [
    source 3
    target 70
    weight 50
  ]
  edge [
    source 3
    target 71
    weight 50
  ]
  edge [
    source 4
    target 13
    weight 1
  ]
  edge [
    source 4
    target 14
    weight 1
  ]
  edge [
    source 4
    target 15
    weight 1
  ]
  edge [
    source 4
    target 16
    weight 1
  ]
  edge [
    source 4
    target 17
    weight 1
  ]
  edge [
    source 4
    target 18
    weight 1
  ]
  edge [
    source 4
    target 19
    weight 1
  ]
  edge [
    source 4
    target 20
    weight 1
  ]
  edge [
    source 4
    target 21
    weight 1
  ]
  edge [
    source 4
    target 22
    weight 1
  ]
  edge [
    source 4
    target 23
    weight 1
  ]
  edge [
    source 4
    target 24
    weight 1
  ]
  edge [
    source 4
    target 25
    weight 1
  ]
  edge [
    source 4
    target 26
    weight 1
  ]
  edge [
    source 4
    target 27
    weight 1
  ]
  edge [
    source 4
    target 28
    weight 1
  ]
  edge [
    source 4
    target 29
    weight 1
  ]
  edge [
    source 4
    target 30
    weight 1
  ]
  edge [
    source 4
    target 31
    weight 1
  ]
  edge [
    source 4
    target 32
    weight 1
  ]
  edge [
    source 4
    target 33
    weight 1
  ]
  edge [
    source 4
    target 34
    weight 1
  ]
  edge [
    source 4
    target 35
    weight 1
  ]
  edge [
    source 4
    target 36
    weight 1
  ]
  edge [
    source 4
    target 37
    weight 1
  ]
  edge [
    source 4
    target 38
    weight 1
  ]
  edge [
    source 4
    target 39
    weight 1
  ]
  edge [
    source 4
    target 40
    weight 1
  ]
  edge [
    source 4
    target 41
    weight 1
  ]
  edge [
    source 4
    target 42
    weight 1
  ]
  edge [
    source 4
    target 43
    weight 1
  ]
  edge [
    source 4
    target 44
    weight 1
  ]
  edge [
    source 4
    target 45
    weight 1
  ]
  edge [
    source 4
    target 47
    weight 1
  ]
  edge [
    source 4
    target 48
    weight 1
  ]
  edge [
    source 4
    target 49
    weight 1
  ]
  edge [
    source 4
    target 50
    weight 1
  ]
  edge [
    source 4
    target 51
    weight 1
  ]
  edge [
    source 4
    target 52
    weight 1
  ]
  edge [
    source 4
    target 53
    weight 1
  ]
  edge [
    source 4
    target 54
    weight 1
  ]
  edge [
    source 4
    target 55
    weight 1
  ]
  edge [
    source 4
    target 56
    weight 1
  ]
  edge [
    source 4
    target 57
    weight 1
  ]
  edge [
    source 4
    target 62
    weight 1
  ]
  edge [
    source 4
    target 63
    weight 1
  ]
  edge [
    source 4
    target 64
    weight 1
  ]
  edge [
    source 4
    target 65
    weight 1
  ]
  edge [
    source 4
    target 66
    weight 1
  ]
  edge [
    source 5
    target 13
    weight 1
  ]
  edge [
    source 5
    target 14
    weight 1
  ]
  edge [
    source 5
    target 15
    weight 1
  ]
  edge [
    source 5
    target 16
    weight 1
  ]
  edge [
    source 5
    target 17
    weight 1
  ]
  edge [
    source 5
    target 18
    weight 1
  ]
  edge [
    source 5
    target 19
    weight 1
  ]
  edge [
    source 5
    target 20
    weight 1
  ]
  edge [
    source 5
    target 21
    weight 1
  ]
  edge [
    source 5
    target 22
    weight 1
  ]
  edge [
    source 5
    target 23
    weight 1
  ]
  edge [
    source 5
    target 24
    weight 1
  ]
  edge [
    source 5
    target 25
    weight 1
  ]
  edge [
    source 5
    target 26
    weight 1
  ]
  edge [
    source 5
    target 27
    weight 1
  ]
  edge [
    source 5
    target 28
    weight 1
  ]
  edge [
    source 5
    target 29
    weight 1
  ]
  edge [
    source 5
    target 30
    weight 1
  ]
  edge [
    source 5
    target 31
    weight 1
  ]
  edge [
    source 5
    target 32
    weight 1
  ]
  edge [
    source 5
    target 33
    weight 1
  ]
  edge [
    source 5
    target 34
    weight 1
  ]
  edge [
    source 5
    target 35
    weight 1
  ]
  edge [
    source 5
    target 36
    weight 1
  ]
  edge [
    source 5
    target 37
    weight 1
  ]
  edge [
    source 5
    target 38
    weight 1
  ]
  edge [
    source 5
    target 39
    weight 1
  ]
  edge [
    source 5
    target 40
    weight 1
  ]
  edge [
    source 5
    target 41
    weight 1
  ]
  edge [
    source 5
    target 42
    weight 1
  ]
  edge [
    source 5
    target 43
    weight 1
  ]
  edge [
    source 5
    target 44
    weight 1
  ]
  edge [
    source 5
    target 45
    weight 1
  ]
  edge [
    source 5
    target 47
    weight 1
  ]
  edge [
    source 5
    target 48
    weight 1
  ]
  edge [
    source 5
    target 49
    weight 1
  ]
  edge [
    source 5
    target 50
    weight 1
  ]
  edge [
    source 5
    target 51
    weight 1
  ]
  edge [
    source 5
    target 52
    weight 1
  ]
  edge [
    source 5
    target 53
    weight 1
  ]
  edge [
    source 5
    target 54
    weight 1
  ]
  edge [
    source 5
    target 55
    weight 1
  ]
  edge [
    source 5
    target 56
    weight 1
  ]
  edge [
    source 5
    target 57
    weight 1
  ]
  edge [
    source 5
    target 62
    weight 1
  ]
  edge [
    source 5
    target 63
    weight 1
  ]
  edge [
    source 5
    target 64
    weight 1
  ]
  edge [
    source 5
    target 65
    weight 1
  ]
  edge [
    source 5
    target 66
    weight 1
  ]
  edge [
    source 6
    target 13
    weight 1
  ]
  edge [
    source 6
    target 14
    weight 1
  ]
  edge [
    source 6
    target 15
    weight 1
  ]
  edge [
    source 6
    target 16
    weight 1
  ]
  edge [
    source 6
    target 17
    weight 1
  ]
  edge [
    source 6
    target 18
    weight 1
  ]
  edge [
    source 6
    target 19
    weight 1
  ]
  edge [
    source 6
    target 20
    weight 1
  ]
  edge [
    source 6
    target 21
    weight 1
  ]
  edge [
    source 6
    target 22
    weight 1
  ]
  edge [
    source 6
    target 23
    weight 1
  ]
  edge [
    source 6
    target 24
    weight 1
  ]
  edge [
    source 6
    target 25
    weight 1
  ]
  edge [
    source 6
    target 26
    weight 1
  ]
  edge [
    source 6
    target 27
    weight 1
  ]
  edge [
    source 6
    target 28
    weight 1
  ]
  edge [
    source 6
    target 29
    weight 1
  ]
  edge [
    source 6
    target 42
    weight 1
  ]
  edge [
    source 6
    target 44
    weight 1
  ]
  edge [
    source 6
    target 43
    weight 1
  ]
  edge [
    source 6
    target 45
    weight 1
  ]
  edge [
    source 6
    target 33
    weight 1
  ]
  edge [
    source 6
    target 30
    weight 1
  ]
  edge [
    source 6
    target 35
    weight 1
  ]
  edge [
    source 6
    target 34
    weight 1
  ]
  edge [
    source 6
    target 37
    weight 1
  ]
  edge [
    source 6
    target 36
    weight 1
  ]
  edge [
    source 6
    target 38
    weight 1
  ]
  edge [
    source 6
    target 39
    weight 1
  ]
  edge [
    source 6
    target 40
    weight 1
  ]
  edge [
    source 6
    target 41
    weight 1
  ]
  edge [
    source 6
    target 32
    weight 1
  ]
  edge [
    source 6
    target 31
    weight 1
  ]
  edge [
    source 6
    target 47
    weight 1
  ]
  edge [
    source 6
    target 48
    weight 1
  ]
  edge [
    source 6
    target 52
    weight 1
  ]
  edge [
    source 6
    target 51
    weight 1
  ]
  edge [
    source 6
    target 50
    weight 1
  ]
  edge [
    source 6
    target 49
    weight 1
  ]
  edge [
    source 6
    target 53
    weight 1
  ]
  edge [
    source 6
    target 54
    weight 1
  ]
  edge [
    source 6
    target 55
    weight 1
  ]
  edge [
    source 6
    target 56
    weight 1
  ]
  edge [
    source 6
    target 57
    weight 1
  ]
  edge [
    source 6
    target 62
    weight 1
  ]
  edge [
    source 6
    target 63
    weight 1
  ]
  edge [
    source 6
    target 64
    weight 1
  ]
  edge [
    source 6
    target 65
    weight 1
  ]
  edge [
    source 6
    target 66
    weight 1
  ]
  edge [
    source 7
    target 13
    weight 1
  ]
  edge [
    source 7
    target 14
    weight 1
  ]
  edge [
    source 7
    target 15
    weight 1
  ]
  edge [
    source 7
    target 16
    weight 1
  ]
  edge [
    source 7
    target 17
    weight 1
  ]
  edge [
    source 7
    target 18
    weight 1
  ]
  edge [
    source 7
    target 19
    weight 1
  ]
  edge [
    source 7
    target 20
    weight 1
  ]
  edge [
    source 7
    target 21
    weight 1
  ]
  edge [
    source 7
    target 22
    weight 1
  ]
  edge [
    source 7
    target 23
    weight 1
  ]
  edge [
    source 7
    target 24
    weight 1
  ]
  edge [
    source 7
    target 25
    weight 1
  ]
  edge [
    source 7
    target 26
    weight 1
  ]
  edge [
    source 7
    target 27
    weight 1
  ]
  edge [
    source 7
    target 28
    weight 1
  ]
  edge [
    source 7
    target 29
    weight 1
  ]
  edge [
    source 7
    target 33
    weight 1
  ]
  edge [
    source 7
    target 30
    weight 1
  ]
  edge [
    source 7
    target 31
    weight 1
  ]
  edge [
    source 7
    target 32
    weight 1
  ]
  edge [
    source 7
    target 34
    weight 1
  ]
  edge [
    source 7
    target 35
    weight 1
  ]
  edge [
    source 7
    target 36
    weight 1
  ]
  edge [
    source 7
    target 37
    weight 1
  ]
  edge [
    source 7
    target 38
    weight 1
  ]
  edge [
    source 7
    target 39
    weight 1
  ]
  edge [
    source 7
    target 40
    weight 1
  ]
  edge [
    source 7
    target 41
    weight 1
  ]
  edge [
    source 7
    target 42
    weight 1
  ]
  edge [
    source 7
    target 43
    weight 1
  ]
  edge [
    source 7
    target 44
    weight 1
  ]
  edge [
    source 7
    target 45
    weight 1
  ]
  edge [
    source 7
    target 47
    weight 1
  ]
  edge [
    source 7
    target 48
    weight 1
  ]
  edge [
    source 7
    target 49
    weight 1
  ]
  edge [
    source 7
    target 50
    weight 1
  ]
  edge [
    source 7
    target 51
    weight 1
  ]
  edge [
    source 7
    target 52
    weight 1
  ]
  edge [
    source 7
    target 53
    weight 1
  ]
  edge [
    source 7
    target 54
    weight 1
  ]
  edge [
    source 7
    target 55
    weight 1
  ]
  edge [
    source 7
    target 56
    weight 1
  ]
  edge [
    source 7
    target 57
    weight 1
  ]
  edge [
    source 7
    target 63
    weight 1
  ]
  edge [
    source 7
    target 64
    weight 1
  ]
  edge [
    source 7
    target 62
    weight 1
  ]
  edge [
    source 7
    target 65
    weight 1
  ]
  edge [
    source 7
    target 66
    weight 1
  ]
  edge [
    source 8
    target 13
    weight 1
  ]
  edge [
    source 8
    target 14
    weight 1
  ]
  edge [
    source 8
    target 15
    weight 1
  ]
  edge [
    source 8
    target 16
    weight 1
  ]
  edge [
    source 8
    target 17
    weight 1
  ]
  edge [
    source 8
    target 18
    weight 1
  ]
  edge [
    source 8
    target 19
    weight 1
  ]
  edge [
    source 8
    target 20
    weight 1
  ]
  edge [
    source 8
    target 21
    weight 1
  ]
  edge [
    source 8
    target 22
    weight 1
  ]
  edge [
    source 8
    target 23
    weight 1
  ]
  edge [
    source 8
    target 24
    weight 1
  ]
  edge [
    source 8
    target 25
    weight 1
  ]
  edge [
    source 8
    target 26
    weight 1
  ]
  edge [
    source 8
    target 27
    weight 1
  ]
  edge [
    source 8
    target 28
    weight 1
  ]
  edge [
    source 8
    target 29
    weight 1
  ]
  edge [
    source 8
    target 33
    weight 1
  ]
  edge [
    source 8
    target 30
    weight 1
  ]
  edge [
    source 8
    target 31
    weight 1
  ]
  edge [
    source 8
    target 32
    weight 1
  ]
  edge [
    source 8
    target 34
    weight 1
  ]
  edge [
    source 8
    target 35
    weight 1
  ]
  edge [
    source 8
    target 36
    weight 1
  ]
  edge [
    source 8
    target 37
    weight 1
  ]
  edge [
    source 8
    target 38
    weight 1
  ]
  edge [
    source 8
    target 39
    weight 1
  ]
  edge [
    source 8
    target 40
    weight 1
  ]
  edge [
    source 8
    target 41
    weight 1
  ]
  edge [
    source 8
    target 42
    weight 1
  ]
  edge [
    source 8
    target 43
    weight 1
  ]
  edge [
    source 8
    target 44
    weight 1
  ]
  edge [
    source 8
    target 45
    weight 1
  ]
  edge [
    source 8
    target 47
    weight 1
  ]
  edge [
    source 8
    target 48
    weight 1
  ]
  edge [
    source 8
    target 49
    weight 1
  ]
  edge [
    source 8
    target 50
    weight 1
  ]
  edge [
    source 8
    target 51
    weight 1
  ]
  edge [
    source 8
    target 52
    weight 1
  ]
  edge [
    source 8
    target 53
    weight 1
  ]
  edge [
    source 8
    target 54
    weight 1
  ]
  edge [
    source 8
    target 55
    weight 1
  ]
  edge [
    source 8
    target 56
    weight 1
  ]
  edge [
    source 8
    target 57
    weight 1
  ]
  edge [
    source 8
    target 62
    weight 1
  ]
  edge [
    source 8
    target 63
    weight 1
  ]
  edge [
    source 8
    target 64
    weight 1
  ]
  edge [
    source 8
    target 65
    weight 1
  ]
  edge [
    source 8
    target 66
    weight 1
  ]
  edge [
    source 9
    target 13
    weight 1
  ]
  edge [
    source 9
    target 14
    weight 1
  ]
  edge [
    source 9
    target 15
    weight 1
  ]
  edge [
    source 9
    target 16
    weight 1
  ]
  edge [
    source 9
    target 17
    weight 1
  ]
  edge [
    source 9
    target 18
    weight 1
  ]
  edge [
    source 9
    target 19
    weight 1
  ]
  edge [
    source 9
    target 20
    weight 1
  ]
  edge [
    source 9
    target 21
    weight 1
  ]
  edge [
    source 9
    target 22
    weight 1
  ]
  edge [
    source 9
    target 23
    weight 1
  ]
  edge [
    source 9
    target 24
    weight 1
  ]
  edge [
    source 9
    target 25
    weight 1
  ]
  edge [
    source 9
    target 26
    weight 1
  ]
  edge [
    source 9
    target 27
    weight 1
  ]
  edge [
    source 9
    target 28
    weight 1
  ]
  edge [
    source 9
    target 29
    weight 1
  ]
  edge [
    source 9
    target 33
    weight 1
  ]
  edge [
    source 9
    target 30
    weight 1
  ]
  edge [
    source 9
    target 31
    weight 1
  ]
  edge [
    source 9
    target 32
    weight 1
  ]
  edge [
    source 9
    target 34
    weight 1
  ]
  edge [
    source 9
    target 35
    weight 1
  ]
  edge [
    source 9
    target 36
    weight 1
  ]
  edge [
    source 9
    target 37
    weight 1
  ]
  edge [
    source 9
    target 38
    weight 1
  ]
  edge [
    source 9
    target 39
    weight 1
  ]
  edge [
    source 9
    target 40
    weight 1
  ]
  edge [
    source 9
    target 41
    weight 1
  ]
  edge [
    source 9
    target 42
    weight 1
  ]
  edge [
    source 9
    target 43
    weight 1
  ]
  edge [
    source 9
    target 44
    weight 1
  ]
  edge [
    source 9
    target 45
    weight 1
  ]
  edge [
    source 9
    target 47
    weight 1
  ]
  edge [
    source 9
    target 48
    weight 1
  ]
  edge [
    source 9
    target 49
    weight 1
  ]
  edge [
    source 9
    target 50
    weight 1
  ]
  edge [
    source 9
    target 51
    weight 1
  ]
  edge [
    source 9
    target 52
    weight 1
  ]
  edge [
    source 9
    target 53
    weight 1
  ]
  edge [
    source 9
    target 54
    weight 1
  ]
  edge [
    source 9
    target 55
    weight 1
  ]
  edge [
    source 9
    target 56
    weight 1
  ]
  edge [
    source 9
    target 57
    weight 1
  ]
  edge [
    source 9
    target 64
    weight 1
  ]
  edge [
    source 9
    target 62
    weight 1
  ]
  edge [
    source 9
    target 63
    weight 1
  ]
  edge [
    source 9
    target 65
    weight 1
  ]
  edge [
    source 9
    target 66
    weight 1
  ]
  edge [
    source 10
    target 13
    weight 1
  ]
  edge [
    source 10
    target 14
    weight 1
  ]
  edge [
    source 10
    target 15
    weight 1
  ]
  edge [
    source 10
    target 16
    weight 1
  ]
  edge [
    source 10
    target 17
    weight 1
  ]
  edge [
    source 10
    target 18
    weight 1
  ]
  edge [
    source 10
    target 19
    weight 1
  ]
  edge [
    source 10
    target 20
    weight 1
  ]
  edge [
    source 10
    target 21
    weight 1
  ]
  edge [
    source 10
    target 22
    weight 1
  ]
  edge [
    source 10
    target 23
    weight 1
  ]
  edge [
    source 10
    target 24
    weight 1
  ]
  edge [
    source 10
    target 25
    weight 1
  ]
  edge [
    source 10
    target 26
    weight 1
  ]
  edge [
    source 10
    target 27
    weight 1
  ]
  edge [
    source 10
    target 28
    weight 1
  ]
  edge [
    source 10
    target 29
    weight 1
  ]
  edge [
    source 10
    target 33
    weight 1
  ]
  edge [
    source 10
    target 30
    weight 1
  ]
  edge [
    source 10
    target 34
    weight 1
  ]
  edge [
    source 10
    target 35
    weight 1
  ]
  edge [
    source 10
    target 36
    weight 1
  ]
  edge [
    source 10
    target 37
    weight 1
  ]
  edge [
    source 10
    target 38
    weight 1
  ]
  edge [
    source 10
    target 39
    weight 1
  ]
  edge [
    source 10
    target 40
    weight 1
  ]
  edge [
    source 10
    target 32
    weight 1
  ]
  edge [
    source 10
    target 31
    weight 1
  ]
  edge [
    source 10
    target 41
    weight 1
  ]
  edge [
    source 10
    target 42
    weight 1
  ]
  edge [
    source 10
    target 44
    weight 1
  ]
  edge [
    source 10
    target 43
    weight 1
  ]
  edge [
    source 10
    target 45
    weight 1
  ]
  edge [
    source 10
    target 47
    weight 1
  ]
  edge [
    source 10
    target 48
    weight 1
  ]
  edge [
    source 10
    target 52
    weight 1
  ]
  edge [
    source 10
    target 51
    weight 1
  ]
  edge [
    source 10
    target 50
    weight 1
  ]
  edge [
    source 10
    target 49
    weight 1
  ]
  edge [
    source 10
    target 53
    weight 1
  ]
  edge [
    source 10
    target 54
    weight 1
  ]
  edge [
    source 10
    target 55
    weight 1
  ]
  edge [
    source 10
    target 56
    weight 1
  ]
  edge [
    source 10
    target 57
    weight 1
  ]
  edge [
    source 10
    target 62
    weight 1
  ]
  edge [
    source 10
    target 63
    weight 1
  ]
  edge [
    source 10
    target 64
    weight 1
  ]
  edge [
    source 10
    target 65
    weight 1
  ]
  edge [
    source 10
    target 66
    weight 1
  ]
  edge [
    source 11
    target 13
    weight 1
  ]
  edge [
    source 11
    target 14
    weight 1
  ]
  edge [
    source 11
    target 29
    weight 1
  ]
  edge [
    source 11
    target 42
    weight 1
  ]
  edge [
    source 11
    target 44
    weight 1
  ]
  edge [
    source 11
    target 43
    weight 1
  ]
  edge [
    source 11
    target 45
    weight 1
  ]
  edge [
    source 11
    target 47
    weight 1
  ]
  edge [
    source 11
    target 48
    weight 1
  ]
  edge [
    source 11
    target 52
    weight 1
  ]
  edge [
    source 11
    target 51
    weight 1
  ]
  edge [
    source 11
    target 50
    weight 1
  ]
  edge [
    source 11
    target 49
    weight 1
  ]
  edge [
    source 11
    target 53
    weight 1
  ]
  edge [
    source 11
    target 54
    weight 1
  ]
  edge [
    source 11
    target 55
    weight 1
  ]
  edge [
    source 11
    target 56
    weight 1
  ]
  edge [
    source 11
    target 57
    weight 1
  ]
  edge [
    source 11
    target 62
    weight 1
  ]
  edge [
    source 11
    target 63
    weight 1
  ]
  edge [
    source 11
    target 64
    weight 1
  ]
  edge [
    source 11
    target 65
    weight 1
  ]
  edge [
    source 11
    target 66
    weight 1
  ]
  edge [
    source 11
    target 32
    weight 1
  ]
  edge [
    source 11
    target 34
    weight 1
  ]
  edge [
    source 11
    target 35
    weight 1
  ]
  edge [
    source 11
    target 37
    weight 1
  ]
  edge [
    source 11
    target 36
    weight 1
  ]
  edge [
    source 11
    target 39
    weight 1
  ]
  edge [
    source 11
    target 38
    weight 1
  ]
  edge [
    source 11
    target 40
    weight 1
  ]
  edge [
    source 11
    target 31
    weight 1
  ]
  edge [
    source 11
    target 33
    weight 1
  ]
  edge [
    source 11
    target 30
    weight 1
  ]
  edge [
    source 11
    target 41
    weight 1
  ]
  edge [
    source 11
    target 25
    weight 1
  ]
  edge [
    source 11
    target 20
    weight 1
  ]
  edge [
    source 11
    target 17
    weight 1
  ]
  edge [
    source 11
    target 24
    weight 1
  ]
  edge [
    source 11
    target 21
    weight 1
  ]
  edge [
    source 11
    target 22
    weight 1
  ]
  edge [
    source 11
    target 19
    weight 1
  ]
  edge [
    source 11
    target 18
    weight 1
  ]
  edge [
    source 11
    target 23
    weight 1
  ]
  edge [
    source 11
    target 28
    weight 1
  ]
  edge [
    source 11
    target 26
    weight 1
  ]
  edge [
    source 11
    target 27
    weight 1
  ]
  edge [
    source 11
    target 15
    weight 1
  ]
  edge [
    source 11
    target 16
    weight 1
  ]
  edge [
    source 12
    target 13
    weight 1
  ]
  edge [
    source 12
    target 14
    weight 1
  ]
  edge [
    source 12
    target 15
    weight 1
  ]
  edge [
    source 12
    target 16
    weight 1
  ]
  edge [
    source 12
    target 17
    weight 1
  ]
  edge [
    source 12
    target 18
    weight 1
  ]
  edge [
    source 12
    target 19
    weight 1
  ]
  edge [
    source 12
    target 20
    weight 1
  ]
  edge [
    source 12
    target 21
    weight 1
  ]
  edge [
    source 12
    target 22
    weight 1
  ]
  edge [
    source 12
    target 23
    weight 1
  ]
  edge [
    source 12
    target 24
    weight 1
  ]
  edge [
    source 12
    target 25
    weight 1
  ]
  edge [
    source 12
    target 26
    weight 1
  ]
  edge [
    source 12
    target 27
    weight 1
  ]
  edge [
    source 12
    target 28
    weight 1
  ]
  edge [
    source 12
    target 29
    weight 1
  ]
  edge [
    source 12
    target 30
    weight 1
  ]
  edge [
    source 12
    target 31
    weight 1
  ]
  edge [
    source 12
    target 32
    weight 1
  ]
  edge [
    source 12
    target 33
    weight 1
  ]
  edge [
    source 12
    target 34
    weight 1
  ]
  edge [
    source 12
    target 35
    weight 1
  ]
  edge [
    source 12
    target 36
    weight 1
  ]
  edge [
    source 12
    target 37
    weight 1
  ]
  edge [
    source 12
    target 38
    weight 1
  ]
  edge [
    source 12
    target 39
    weight 1
  ]
  edge [
    source 12
    target 40
    weight 1
  ]
  edge [
    source 12
    target 44
    weight 1
  ]
  edge [
    source 12
    target 43
    weight 1
  ]
  edge [
    source 12
    target 42
    weight 1
  ]
  edge [
    source 12
    target 45
    weight 1
  ]
  edge [
    source 12
    target 47
    weight 1
  ]
  edge [
    source 12
    target 52
    weight 1
  ]
  edge [
    source 12
    target 48
    weight 1
  ]
  edge [
    source 12
    target 50
    weight 1
  ]
  edge [
    source 12
    target 51
    weight 1
  ]
  edge [
    source 12
    target 49
    weight 1
  ]
  edge [
    source 12
    target 53
    weight 1
  ]
  edge [
    source 12
    target 54
    weight 1
  ]
  edge [
    source 12
    target 55
    weight 1
  ]
  edge [
    source 12
    target 56
    weight 1
  ]
  edge [
    source 12
    target 57
    weight 1
  ]
  edge [
    source 12
    target 66
    weight 1
  ]
  edge [
    source 12
    target 64
    weight 1
  ]
  edge [
    source 12
    target 62
    weight 1
  ]
  edge [
    source 12
    target 63
    weight 1
  ]
  edge [
    source 12
    target 65
    weight 1
  ]
  edge [
    source 12
    target 41
    weight 1
  ]
  edge [
    source 13
    target 58
    weight 1
  ]
  edge [
    source 13
    target 59
    weight 1
  ]
  edge [
    source 13
    target 61
    weight 1
  ]
  edge [
    source 13
    target 60
    weight 1
  ]
  edge [
    source 13
    target 46
    weight 1
  ]
  edge [
    source 13
    target 67
    weight 1
  ]
  edge [
    source 13
    target 68
    weight 1
  ]
  edge [
    source 13
    target 69
    weight 1
  ]
  edge [
    source 13
    target 70
    weight 1
  ]
  edge [
    source 13
    target 71
    weight 1
  ]
  edge [
    source 14
    target 58
    weight 1
  ]
  edge [
    source 14
    target 59
    weight 1
  ]
  edge [
    source 14
    target 60
    weight 1
  ]
  edge [
    source 14
    target 61
    weight 1
  ]
  edge [
    source 14
    target 46
    weight 1
  ]
  edge [
    source 14
    target 67
    weight 1
  ]
  edge [
    source 14
    target 68
    weight 1
  ]
  edge [
    source 14
    target 69
    weight 1
  ]
  edge [
    source 14
    target 70
    weight 1
  ]
  edge [
    source 14
    target 71
    weight 1
  ]
  edge [
    source 15
    target 58
    weight 1
  ]
  edge [
    source 15
    target 59
    weight 1
  ]
  edge [
    source 15
    target 60
    weight 1
  ]
  edge [
    source 15
    target 61
    weight 1
  ]
  edge [
    source 15
    target 46
    weight 1
  ]
  edge [
    source 15
    target 67
    weight 1
  ]
  edge [
    source 15
    target 68
    weight 1
  ]
  edge [
    source 15
    target 69
    weight 1
  ]
  edge [
    source 15
    target 70
    weight 1
  ]
  edge [
    source 15
    target 71
    weight 1
  ]
  edge [
    source 16
    target 59
    weight 1
  ]
  edge [
    source 16
    target 58
    weight 1
  ]
  edge [
    source 16
    target 61
    weight 1
  ]
  edge [
    source 16
    target 60
    weight 1
  ]
  edge [
    source 16
    target 46
    weight 1
  ]
  edge [
    source 16
    target 67
    weight 1
  ]
  edge [
    source 16
    target 68
    weight 1
  ]
  edge [
    source 16
    target 69
    weight 1
  ]
  edge [
    source 16
    target 70
    weight 1
  ]
  edge [
    source 16
    target 71
    weight 1
  ]
  edge [
    source 17
    target 58
    weight 1
  ]
  edge [
    source 17
    target 59
    weight 1
  ]
  edge [
    source 17
    target 60
    weight 1
  ]
  edge [
    source 17
    target 61
    weight 1
  ]
  edge [
    source 17
    target 46
    weight 1
  ]
  edge [
    source 17
    target 67
    weight 1
  ]
  edge [
    source 17
    target 68
    weight 1
  ]
  edge [
    source 17
    target 69
    weight 1
  ]
  edge [
    source 17
    target 70
    weight 1
  ]
  edge [
    source 17
    target 71
    weight 1
  ]
  edge [
    source 18
    target 59
    weight 1
  ]
  edge [
    source 18
    target 58
    weight 1
  ]
  edge [
    source 18
    target 61
    weight 1
  ]
  edge [
    source 18
    target 60
    weight 1
  ]
  edge [
    source 18
    target 46
    weight 1
  ]
  edge [
    source 18
    target 67
    weight 1
  ]
  edge [
    source 18
    target 68
    weight 1
  ]
  edge [
    source 18
    target 69
    weight 1
  ]
  edge [
    source 18
    target 70
    weight 1
  ]
  edge [
    source 18
    target 71
    weight 1
  ]
  edge [
    source 19
    target 59
    weight 1
  ]
  edge [
    source 19
    target 58
    weight 1
  ]
  edge [
    source 19
    target 60
    weight 1
  ]
  edge [
    source 19
    target 61
    weight 1
  ]
  edge [
    source 19
    target 46
    weight 1
  ]
  edge [
    source 19
    target 67
    weight 1
  ]
  edge [
    source 19
    target 68
    weight 1
  ]
  edge [
    source 19
    target 69
    weight 1
  ]
  edge [
    source 19
    target 70
    weight 1
  ]
  edge [
    source 19
    target 71
    weight 1
  ]
  edge [
    source 20
    target 59
    weight 1
  ]
  edge [
    source 20
    target 58
    weight 1
  ]
  edge [
    source 20
    target 61
    weight 1
  ]
  edge [
    source 20
    target 60
    weight 1
  ]
  edge [
    source 20
    target 46
    weight 1
  ]
  edge [
    source 20
    target 67
    weight 1
  ]
  edge [
    source 20
    target 68
    weight 1
  ]
  edge [
    source 20
    target 69
    weight 1
  ]
  edge [
    source 20
    target 70
    weight 1
  ]
  edge [
    source 20
    target 71
    weight 1
  ]
  edge [
    source 21
    target 59
    weight 1
  ]
  edge [
    source 21
    target 58
    weight 1
  ]
  edge [
    source 21
    target 60
    weight 1
  ]
  edge [
    source 21
    target 61
    weight 1
  ]
  edge [
    source 21
    target 46
    weight 1
  ]
  edge [
    source 21
    target 67
    weight 1
  ]
  edge [
    source 21
    target 68
    weight 1
  ]
  edge [
    source 21
    target 69
    weight 1
  ]
  edge [
    source 21
    target 70
    weight 1
  ]
  edge [
    source 21
    target 71
    weight 1
  ]
  edge [
    source 22
    target 59
    weight 1
  ]
  edge [
    source 22
    target 58
    weight 1
  ]
  edge [
    source 22
    target 61
    weight 1
  ]
  edge [
    source 22
    target 60
    weight 1
  ]
  edge [
    source 22
    target 46
    weight 1
  ]
  edge [
    source 22
    target 67
    weight 1
  ]
  edge [
    source 22
    target 68
    weight 1
  ]
  edge [
    source 22
    target 69
    weight 1
  ]
  edge [
    source 22
    target 70
    weight 1
  ]
  edge [
    source 22
    target 71
    weight 1
  ]
  edge [
    source 23
    target 58
    weight 1
  ]
  edge [
    source 23
    target 59
    weight 1
  ]
  edge [
    source 23
    target 60
    weight 1
  ]
  edge [
    source 23
    target 61
    weight 1
  ]
  edge [
    source 23
    target 46
    weight 1
  ]
  edge [
    source 23
    target 67
    weight 1
  ]
  edge [
    source 23
    target 68
    weight 1
  ]
  edge [
    source 23
    target 69
    weight 1
  ]
  edge [
    source 23
    target 70
    weight 1
  ]
  edge [
    source 23
    target 71
    weight 1
  ]
  edge [
    source 24
    target 59
    weight 1
  ]
  edge [
    source 24
    target 58
    weight 1
  ]
  edge [
    source 24
    target 61
    weight 1
  ]
  edge [
    source 24
    target 60
    weight 1
  ]
  edge [
    source 24
    target 46
    weight 1
  ]
  edge [
    source 24
    target 67
    weight 1
  ]
  edge [
    source 24
    target 68
    weight 1
  ]
  edge [
    source 24
    target 69
    weight 1
  ]
  edge [
    source 24
    target 70
    weight 1
  ]
  edge [
    source 24
    target 71
    weight 1
  ]
  edge [
    source 25
    target 59
    weight 1
  ]
  edge [
    source 25
    target 58
    weight 1
  ]
  edge [
    source 25
    target 61
    weight 1
  ]
  edge [
    source 25
    target 60
    weight 1
  ]
  edge [
    source 25
    target 46
    weight 1
  ]
  edge [
    source 25
    target 67
    weight 1
  ]
  edge [
    source 25
    target 68
    weight 1
  ]
  edge [
    source 25
    target 69
    weight 1
  ]
  edge [
    source 25
    target 70
    weight 1
  ]
  edge [
    source 25
    target 71
    weight 1
  ]
  edge [
    source 26
    target 58
    weight 1
  ]
  edge [
    source 26
    target 59
    weight 1
  ]
  edge [
    source 26
    target 60
    weight 1
  ]
  edge [
    source 26
    target 61
    weight 1
  ]
  edge [
    source 26
    target 46
    weight 1
  ]
  edge [
    source 26
    target 67
    weight 1
  ]
  edge [
    source 26
    target 68
    weight 1
  ]
  edge [
    source 26
    target 69
    weight 1
  ]
  edge [
    source 26
    target 70
    weight 1
  ]
  edge [
    source 26
    target 71
    weight 1
  ]
  edge [
    source 27
    target 59
    weight 1
  ]
  edge [
    source 27
    target 58
    weight 1
  ]
  edge [
    source 27
    target 61
    weight 1
  ]
  edge [
    source 27
    target 60
    weight 1
  ]
  edge [
    source 27
    target 46
    weight 1
  ]
  edge [
    source 27
    target 67
    weight 1
  ]
  edge [
    source 27
    target 68
    weight 1
  ]
  edge [
    source 27
    target 69
    weight 1
  ]
  edge [
    source 27
    target 70
    weight 1
  ]
  edge [
    source 27
    target 71
    weight 1
  ]
  edge [
    source 28
    target 59
    weight 1
  ]
  edge [
    source 28
    target 58
    weight 1
  ]
  edge [
    source 28
    target 61
    weight 1
  ]
  edge [
    source 28
    target 60
    weight 1
  ]
  edge [
    source 28
    target 46
    weight 1
  ]
  edge [
    source 28
    target 67
    weight 1
  ]
  edge [
    source 28
    target 68
    weight 1
  ]
  edge [
    source 28
    target 69
    weight 1
  ]
  edge [
    source 28
    target 70
    weight 1
  ]
  edge [
    source 28
    target 71
    weight 1
  ]
  edge [
    source 29
    target 58
    weight 1
  ]
  edge [
    source 29
    target 59
    weight 1
  ]
  edge [
    source 29
    target 61
    weight 1
  ]
  edge [
    source 29
    target 60
    weight 1
  ]
  edge [
    source 29
    target 46
    weight 1
  ]
  edge [
    source 29
    target 67
    weight 1
  ]
  edge [
    source 29
    target 68
    weight 1
  ]
  edge [
    source 29
    target 69
    weight 1
  ]
  edge [
    source 29
    target 70
    weight 1
  ]
  edge [
    source 29
    target 71
    weight 1
  ]
  edge [
    source 30
    target 58
    weight 1
  ]
  edge [
    source 30
    target 59
    weight 1
  ]
  edge [
    source 30
    target 61
    weight 1
  ]
  edge [
    source 30
    target 60
    weight 1
  ]
  edge [
    source 30
    target 46
    weight 1
  ]
  edge [
    source 30
    target 67
    weight 1
  ]
  edge [
    source 30
    target 68
    weight 1
  ]
  edge [
    source 30
    target 69
    weight 1
  ]
  edge [
    source 30
    target 70
    weight 1
  ]
  edge [
    source 30
    target 71
    weight 1
  ]
  edge [
    source 31
    target 58
    weight 1
  ]
  edge [
    source 31
    target 59
    weight 1
  ]
  edge [
    source 31
    target 61
    weight 1
  ]
  edge [
    source 31
    target 60
    weight 1
  ]
  edge [
    source 31
    target 46
    weight 1
  ]
  edge [
    source 31
    target 67
    weight 1
  ]
  edge [
    source 31
    target 68
    weight 1
  ]
  edge [
    source 31
    target 69
    weight 1
  ]
  edge [
    source 31
    target 70
    weight 1
  ]
  edge [
    source 31
    target 71
    weight 1
  ]
  edge [
    source 32
    target 58
    weight 1
  ]
  edge [
    source 32
    target 59
    weight 1
  ]
  edge [
    source 32
    target 60
    weight 1
  ]
  edge [
    source 32
    target 61
    weight 1
  ]
  edge [
    source 32
    target 46
    weight 1
  ]
  edge [
    source 32
    target 67
    weight 1
  ]
  edge [
    source 32
    target 68
    weight 1
  ]
  edge [
    source 32
    target 69
    weight 1
  ]
  edge [
    source 32
    target 70
    weight 1
  ]
  edge [
    source 32
    target 71
    weight 1
  ]
  edge [
    source 33
    target 58
    weight 1
  ]
  edge [
    source 33
    target 59
    weight 1
  ]
  edge [
    source 33
    target 60
    weight 1
  ]
  edge [
    source 33
    target 61
    weight 1
  ]
  edge [
    source 33
    target 46
    weight 1
  ]
  edge [
    source 33
    target 67
    weight 1
  ]
  edge [
    source 33
    target 68
    weight 1
  ]
  edge [
    source 33
    target 69
    weight 1
  ]
  edge [
    source 33
    target 70
    weight 1
  ]
  edge [
    source 33
    target 71
    weight 1
  ]
  edge [
    source 34
    target 58
    weight 1
  ]
  edge [
    source 34
    target 59
    weight 1
  ]
  edge [
    source 34
    target 60
    weight 1
  ]
  edge [
    source 34
    target 61
    weight 1
  ]
  edge [
    source 34
    target 46
    weight 1
  ]
  edge [
    source 34
    target 67
    weight 1
  ]
  edge [
    source 34
    target 68
    weight 1
  ]
  edge [
    source 34
    target 69
    weight 1
  ]
  edge [
    source 34
    target 70
    weight 1
  ]
  edge [
    source 34
    target 71
    weight 1
  ]
  edge [
    source 35
    target 58
    weight 1
  ]
  edge [
    source 35
    target 59
    weight 1
  ]
  edge [
    source 35
    target 61
    weight 1
  ]
  edge [
    source 35
    target 60
    weight 1
  ]
  edge [
    source 35
    target 46
    weight 1
  ]
  edge [
    source 35
    target 67
    weight 1
  ]
  edge [
    source 35
    target 68
    weight 1
  ]
  edge [
    source 35
    target 69
    weight 1
  ]
  edge [
    source 35
    target 70
    weight 1
  ]
  edge [
    source 35
    target 71
    weight 1
  ]
  edge [
    source 36
    target 58
    weight 1
  ]
  edge [
    source 36
    target 59
    weight 1
  ]
  edge [
    source 36
    target 60
    weight 1
  ]
  edge [
    source 36
    target 61
    weight 1
  ]
  edge [
    source 36
    target 46
    weight 1
  ]
  edge [
    source 36
    target 67
    weight 1
  ]
  edge [
    source 36
    target 68
    weight 1
  ]
  edge [
    source 36
    target 69
    weight 1
  ]
  edge [
    source 36
    target 70
    weight 1
  ]
  edge [
    source 36
    target 71
    weight 1
  ]
  edge [
    source 37
    target 58
    weight 1
  ]
  edge [
    source 37
    target 59
    weight 1
  ]
  edge [
    source 37
    target 61
    weight 1
  ]
  edge [
    source 37
    target 60
    weight 1
  ]
  edge [
    source 37
    target 46
    weight 1
  ]
  edge [
    source 37
    target 67
    weight 1
  ]
  edge [
    source 37
    target 68
    weight 1
  ]
  edge [
    source 37
    target 69
    weight 1
  ]
  edge [
    source 37
    target 70
    weight 1
  ]
  edge [
    source 37
    target 71
    weight 1
  ]
  edge [
    source 38
    target 58
    weight 1
  ]
  edge [
    source 38
    target 59
    weight 1
  ]
  edge [
    source 38
    target 60
    weight 1
  ]
  edge [
    source 38
    target 61
    weight 1
  ]
  edge [
    source 38
    target 46
    weight 1
  ]
  edge [
    source 38
    target 67
    weight 1
  ]
  edge [
    source 38
    target 68
    weight 1
  ]
  edge [
    source 38
    target 69
    weight 1
  ]
  edge [
    source 38
    target 70
    weight 1
  ]
  edge [
    source 38
    target 71
    weight 1
  ]
  edge [
    source 39
    target 58
    weight 1
  ]
  edge [
    source 39
    target 59
    weight 1
  ]
  edge [
    source 39
    target 61
    weight 1
  ]
  edge [
    source 39
    target 60
    weight 1
  ]
  edge [
    source 39
    target 46
    weight 1
  ]
  edge [
    source 39
    target 67
    weight 1
  ]
  edge [
    source 39
    target 68
    weight 1
  ]
  edge [
    source 39
    target 69
    weight 1
  ]
  edge [
    source 39
    target 70
    weight 1
  ]
  edge [
    source 39
    target 71
    weight 1
  ]
  edge [
    source 40
    target 58
    weight 1
  ]
  edge [
    source 40
    target 59
    weight 1
  ]
  edge [
    source 40
    target 60
    weight 1
  ]
  edge [
    source 40
    target 61
    weight 1
  ]
  edge [
    source 40
    target 46
    weight 1
  ]
  edge [
    source 40
    target 67
    weight 1
  ]
  edge [
    source 40
    target 68
    weight 1
  ]
  edge [
    source 40
    target 69
    weight 1
  ]
  edge [
    source 40
    target 70
    weight 1
  ]
  edge [
    source 40
    target 71
    weight 1
  ]
  edge [
    source 41
    target 58
    weight 1
  ]
  edge [
    source 41
    target 59
    weight 1
  ]
  edge [
    source 41
    target 61
    weight 1
  ]
  edge [
    source 41
    target 60
    weight 1
  ]
  edge [
    source 41
    target 46
    weight 1
  ]
  edge [
    source 41
    target 67
    weight 1
  ]
  edge [
    source 41
    target 68
    weight 1
  ]
  edge [
    source 41
    target 69
    weight 1
  ]
  edge [
    source 41
    target 70
    weight 1
  ]
  edge [
    source 41
    target 71
    weight 1
  ]
  edge [
    source 42
    target 46
    weight 1
  ]
  edge [
    source 42
    target 59
    weight 1
  ]
  edge [
    source 42
    target 58
    weight 1
  ]
  edge [
    source 42
    target 61
    weight 1
  ]
  edge [
    source 42
    target 60
    weight 1
  ]
  edge [
    source 42
    target 67
    weight 1
  ]
  edge [
    source 42
    target 68
    weight 1
  ]
  edge [
    source 42
    target 69
    weight 1
  ]
  edge [
    source 42
    target 70
    weight 1
  ]
  edge [
    source 42
    target 71
    weight 1
  ]
  edge [
    source 43
    target 46
    weight 1
  ]
  edge [
    source 43
    target 58
    weight 1
  ]
  edge [
    source 43
    target 59
    weight 1
  ]
  edge [
    source 43
    target 60
    weight 1
  ]
  edge [
    source 43
    target 61
    weight 1
  ]
  edge [
    source 43
    target 67
    weight 1
  ]
  edge [
    source 43
    target 68
    weight 1
  ]
  edge [
    source 43
    target 69
    weight 1
  ]
  edge [
    source 43
    target 70
    weight 1
  ]
  edge [
    source 43
    target 71
    weight 1
  ]
  edge [
    source 44
    target 46
    weight 1
  ]
  edge [
    source 44
    target 59
    weight 1
  ]
  edge [
    source 44
    target 58
    weight 1
  ]
  edge [
    source 44
    target 61
    weight 1
  ]
  edge [
    source 44
    target 60
    weight 1
  ]
  edge [
    source 44
    target 67
    weight 1
  ]
  edge [
    source 44
    target 68
    weight 1
  ]
  edge [
    source 44
    target 69
    weight 1
  ]
  edge [
    source 44
    target 70
    weight 1
  ]
  edge [
    source 44
    target 71
    weight 1
  ]
  edge [
    source 45
    target 46
    weight 1
  ]
  edge [
    source 45
    target 58
    weight 1
  ]
  edge [
    source 45
    target 59
    weight 1
  ]
  edge [
    source 45
    target 61
    weight 1
  ]
  edge [
    source 45
    target 60
    weight 1
  ]
  edge [
    source 45
    target 67
    weight 1
  ]
  edge [
    source 45
    target 68
    weight 1
  ]
  edge [
    source 45
    target 69
    weight 1
  ]
  edge [
    source 45
    target 70
    weight 1
  ]
  edge [
    source 45
    target 71
    weight 1
  ]
  edge [
    source 46
    target 47
    weight 1
  ]
  edge [
    source 46
    target 50
    weight 1
  ]
  edge [
    source 46
    target 52
    weight 1
  ]
  edge [
    source 46
    target 49
    weight 1
  ]
  edge [
    source 46
    target 48
    weight 1
  ]
  edge [
    source 46
    target 51
    weight 1
  ]
  edge [
    source 46
    target 53
    weight 1
  ]
  edge [
    source 46
    target 54
    weight 1
  ]
  edge [
    source 46
    target 55
    weight 1
  ]
  edge [
    source 46
    target 56
    weight 1
  ]
  edge [
    source 46
    target 57
    weight 1
  ]
  edge [
    source 46
    target 66
    weight 1
  ]
  edge [
    source 46
    target 64
    weight 1
  ]
  edge [
    source 46
    target 62
    weight 1
  ]
  edge [
    source 46
    target 63
    weight 1
  ]
  edge [
    source 46
    target 65
    weight 1
  ]
  edge [
    source 47
    target 58
    weight 1
  ]
  edge [
    source 47
    target 59
    weight 1
  ]
  edge [
    source 47
    target 60
    weight 1
  ]
  edge [
    source 47
    target 61
    weight 1
  ]
  edge [
    source 47
    target 67
    weight 1
  ]
  edge [
    source 47
    target 68
    weight 1
  ]
  edge [
    source 47
    target 69
    weight 1
  ]
  edge [
    source 47
    target 70
    weight 1
  ]
  edge [
    source 47
    target 71
    weight 1
  ]
  edge [
    source 48
    target 58
    weight 1
  ]
  edge [
    source 48
    target 59
    weight 1
  ]
  edge [
    source 48
    target 60
    weight 1
  ]
  edge [
    source 48
    target 61
    weight 1
  ]
  edge [
    source 48
    target 67
    weight 1
  ]
  edge [
    source 48
    target 68
    weight 1
  ]
  edge [
    source 48
    target 69
    weight 1
  ]
  edge [
    source 48
    target 70
    weight 1
  ]
  edge [
    source 48
    target 71
    weight 1
  ]
  edge [
    source 49
    target 58
    weight 1
  ]
  edge [
    source 49
    target 59
    weight 1
  ]
  edge [
    source 49
    target 60
    weight 1
  ]
  edge [
    source 49
    target 61
    weight 1
  ]
  edge [
    source 49
    target 67
    weight 1
  ]
  edge [
    source 49
    target 68
    weight 1
  ]
  edge [
    source 49
    target 69
    weight 1
  ]
  edge [
    source 49
    target 70
    weight 1
  ]
  edge [
    source 49
    target 71
    weight 1
  ]
  edge [
    source 50
    target 59
    weight 1
  ]
  edge [
    source 50
    target 58
    weight 1
  ]
  edge [
    source 50
    target 61
    weight 1
  ]
  edge [
    source 50
    target 60
    weight 1
  ]
  edge [
    source 50
    target 67
    weight 1
  ]
  edge [
    source 50
    target 68
    weight 1
  ]
  edge [
    source 50
    target 69
    weight 1
  ]
  edge [
    source 50
    target 70
    weight 1
  ]
  edge [
    source 50
    target 71
    weight 1
  ]
  edge [
    source 51
    target 58
    weight 1
  ]
  edge [
    source 51
    target 59
    weight 1
  ]
  edge [
    source 51
    target 60
    weight 1
  ]
  edge [
    source 51
    target 61
    weight 1
  ]
  edge [
    source 51
    target 67
    weight 1
  ]
  edge [
    source 51
    target 68
    weight 1
  ]
  edge [
    source 51
    target 69
    weight 1
  ]
  edge [
    source 51
    target 70
    weight 1
  ]
  edge [
    source 51
    target 71
    weight 1
  ]
  edge [
    source 52
    target 59
    weight 1
  ]
  edge [
    source 52
    target 58
    weight 1
  ]
  edge [
    source 52
    target 61
    weight 1
  ]
  edge [
    source 52
    target 60
    weight 1
  ]
  edge [
    source 52
    target 67
    weight 1
  ]
  edge [
    source 52
    target 68
    weight 1
  ]
  edge [
    source 52
    target 69
    weight 1
  ]
  edge [
    source 52
    target 70
    weight 1
  ]
  edge [
    source 52
    target 71
    weight 1
  ]
  edge [
    source 53
    target 58
    weight 1
  ]
  edge [
    source 53
    target 59
    weight 1
  ]
  edge [
    source 53
    target 61
    weight 1
  ]
  edge [
    source 53
    target 60
    weight 1
  ]
  edge [
    source 53
    target 67
    weight 1
  ]
  edge [
    source 53
    target 68
    weight 1
  ]
  edge [
    source 53
    target 69
    weight 1
  ]
  edge [
    source 53
    target 70
    weight 1
  ]
  edge [
    source 53
    target 71
    weight 1
  ]
  edge [
    source 54
    target 58
    weight 1
  ]
  edge [
    source 54
    target 59
    weight 1
  ]
  edge [
    source 54
    target 60
    weight 1
  ]
  edge [
    source 54
    target 61
    weight 1
  ]
  edge [
    source 54
    target 67
    weight 1
  ]
  edge [
    source 54
    target 68
    weight 1
  ]
  edge [
    source 54
    target 69
    weight 1
  ]
  edge [
    source 54
    target 70
    weight 1
  ]
  edge [
    source 54
    target 71
    weight 1
  ]
  edge [
    source 55
    target 59
    weight 1
  ]
  edge [
    source 55
    target 58
    weight 1
  ]
  edge [
    source 55
    target 60
    weight 1
  ]
  edge [
    source 55
    target 61
    weight 1
  ]
  edge [
    source 55
    target 67
    weight 1
  ]
  edge [
    source 55
    target 68
    weight 1
  ]
  edge [
    source 55
    target 69
    weight 1
  ]
  edge [
    source 55
    target 70
    weight 1
  ]
  edge [
    source 55
    target 71
    weight 1
  ]
  edge [
    source 56
    target 58
    weight 1
  ]
  edge [
    source 56
    target 59
    weight 1
  ]
  edge [
    source 56
    target 60
    weight 1
  ]
  edge [
    source 56
    target 61
    weight 1
  ]
  edge [
    source 56
    target 67
    weight 1
  ]
  edge [
    source 56
    target 68
    weight 1
  ]
  edge [
    source 56
    target 69
    weight 1
  ]
  edge [
    source 56
    target 70
    weight 1
  ]
  edge [
    source 56
    target 71
    weight 1
  ]
  edge [
    source 57
    target 59
    weight 1
  ]
  edge [
    source 57
    target 58
    weight 1
  ]
  edge [
    source 57
    target 60
    weight 1
  ]
  edge [
    source 57
    target 61
    weight 1
  ]
  edge [
    source 57
    target 67
    weight 1
  ]
  edge [
    source 57
    target 68
    weight 1
  ]
  edge [
    source 57
    target 69
    weight 1
  ]
  edge [
    source 57
    target 70
    weight 1
  ]
  edge [
    source 57
    target 71
    weight 1
  ]
  edge [
    source 58
    target 64
    weight 1
  ]
  edge [
    source 58
    target 63
    weight 1
  ]
  edge [
    source 58
    target 62
    weight 1
  ]
  edge [
    source 58
    target 65
    weight 1
  ]
  edge [
    source 58
    target 66
    weight 1
  ]
  edge [
    source 59
    target 64
    weight 1
  ]
  edge [
    source 59
    target 63
    weight 1
  ]
  edge [
    source 59
    target 62
    weight 1
  ]
  edge [
    source 59
    target 65
    weight 1
  ]
  edge [
    source 59
    target 66
    weight 1
  ]
  edge [
    source 60
    target 64
    weight 1
  ]
  edge [
    source 60
    target 63
    weight 1
  ]
  edge [
    source 60
    target 62
    weight 1
  ]
  edge [
    source 60
    target 65
    weight 1
  ]
  edge [
    source 60
    target 66
    weight 1
  ]
  edge [
    source 61
    target 64
    weight 1
  ]
  edge [
    source 61
    target 63
    weight 1
  ]
  edge [
    source 61
    target 62
    weight 1
  ]
  edge [
    source 61
    target 65
    weight 1
  ]
  edge [
    source 61
    target 66
    weight 1
  ]
  edge [
    source 62
    target 67
    weight 1
  ]
  edge [
    source 62
    target 68
    weight 1
  ]
  edge [
    source 62
    target 69
    weight 1
  ]
  edge [
    source 62
    target 70
    weight 1
  ]
  edge [
    source 62
    target 71
    weight 1
  ]
  edge [
    source 63
    target 67
    weight 1
  ]
  edge [
    source 63
    target 68
    weight 1
  ]
  edge [
    source 63
    target 69
    weight 1
  ]
  edge [
    source 63
    target 70
    weight 1
  ]
  edge [
    source 63
    target 71
    weight 1
  ]
  edge [
    source 64
    target 67
    weight 1
  ]
  edge [
    source 64
    target 68
    weight 1
  ]
  edge [
    source 64
    target 69
    weight 1
  ]
  edge [
    source 64
    target 70
    weight 1
  ]
  edge [
    source 64
    target 71
    weight 1
  ]
  edge [
    source 65
    target 67
    weight 1
  ]
  edge [
    source 65
    target 68
    weight 1
  ]
  edge [
    source 65
    target 69
    weight 1
  ]
  edge [
    source 65
    target 70
    weight 1
  ]
  edge [
    source 65
    target 71
    weight 1
  ]
  edge [
    source 66
    target 67
    weight 1
  ]
  edge [
    source 66
    target 68
    weight 1
  ]
  edge [
    source 66
    target 69
    weight 1
  ]
  edge [
    source 66
    target 70
    weight 1
  ]
  edge [
    source 66
    target 71
    weight 1
  ]
]
