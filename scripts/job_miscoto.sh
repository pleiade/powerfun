#!/bin/bash
#SBATCH --job-name=job_miscoto         # Job name
#SBATCH --output=../output/miscoto-%A_%a.out
#SBATCH -e ../error/miscoto_%x.%N.%j.err
#SBATCH --time=5:00:00                 # Time limit hrs:min:sec
#SBATCH --mail-user=chabname.ghasseminedjad@inria.fr         #Receive email on this adress when the job is begin,over,or fail 
#SBATCH --mail-type=END,FAIL                  #Define what we want to receive by email about the job statut
#SBATCH --exclude=arm01

source /home/cghassem/miniconda3/etc/profile.d/conda.sh
conda activate powerfun

miscoto mincom -b ../datas/sbml/ -s  ../datas/seeds_gut.sbml -t ../datas/targets_1500/lipids.sbml --optsol --enumeration --intersection --union -o soup