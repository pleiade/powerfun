#!/bin/bash

# Get arguments
while getopts g:s:b:p:t:r:y:a: flag
do
    case "${flag}" in
        g) GENOTAX=${OPTARG};;
        s) SEEDS_FILE=${OPTARG};;
        b) SBML_DIR=${OPTARG};;
        p) PG_DIR=${OPTARG};;
        t) TMP_DIR=${OPTARG};;
        r) RESULT_DIR=${OPTARG};;
        y) TYPE=${OPTARG};;
        a) TARGET_DIR=${OPTARG};;
    esac
done

# Files need to be in the same order in each directory
BBL_FILE=`ls $PG_DIR*.bbl | head -n $SLURM_ARRAY_TASK_ID | tail -n 1`
TARGET_FILE=`ls $TARGET_DIR*.tsv | head -n $SLURM_ARRAY_TASK_ID | tail -n 1`

powerfun $TYPE -b $SBML_DIR -s $SEEDS_FILE -p $BBL_FILE -gt $GENOTAX -ta $TARGET_FILE -tp $TMP_DIR -od $RESULT_DIR