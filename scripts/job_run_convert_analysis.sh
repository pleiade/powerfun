#!/bin/bash
#SBATCH --job-name=job_convert_analysis         # Job name
#SBATCH --output=../output/convert_analysis_-%A_%a.out
#SBATCH -e ../error/convert_analysis_%x.%N.%j.err
#SBATCH --array=1-7%7 # job array index # or -t 1-3%3
#SBATCH --cpus-per-task=1 # Request that ncpus be allocated per process.
#SBATCH --mem-per-cpu=5gb
#SBATCH --time=5:00:00                 # Time limit hrs:min:sec
#SBATCH --mail-user=chabname.ghasseminedjad@inria.fr         #Receive email on this adress when the job is begin,over,or fail 
#SBATCH --mail-type=END,FAIL                  #Define what we want to receive by email about the job statut
#SBATCH --exclude=arm01

source /home/cghassem/miniconda3/etc/profile.d/conda.sh
conda activate powerfun

# Cluster
./run_convert_analysis.sh -i ../results/analysis/ -r ../results/tsv/
