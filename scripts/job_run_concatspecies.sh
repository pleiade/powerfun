#!/bin/bash
#SBATCH --job-name=job_pf_concat         # Job name
#SBATCH --output=../output/pf_concat-%A_%a.out
#SBATCH -e ../error/pf_concat_%x.%N.%j.err
#SBATCH --cpus-per-task=100 # Request that ncpus be allocated per process.
#SBATCH --mem-per-cpu=16gb
#SBATCH --time=2:00:00                 # Time limit hrs:min:sec
#SBATCH --mail-user=chabname.ghasseminedjad@inria.fr         #Receive email on this adress when the job is begin,over,or fail 
#SBATCH --mail-type=END,FAIL                  #Define what we want to receive by email about the job statut
#SBATCH --exclude=arm01

source /home/cghassem/miniconda3/etc/profile.d/conda.sh
conda activate powerfun

powerfun bconcat -b ../datas/sbml/ -od ../temps/ -of sbml_all_bact_instances.lp -gt ../datas/gut_genome_taxonomy.tsv -c 100