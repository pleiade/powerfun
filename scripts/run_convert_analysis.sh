#!/bin/bash

# Get arguments
while getopts i:r: flag
do
    case "${flag}" in
        i) IN_DIR=${OPTARG};;
        r) RESULT_DIR=${OPTARG};;
    esac
done

# Files need to be in the same order in each directory
JSON_FILE=`ls $IN_DIR*.json | head -n $SLURM_ARRAY_TASK_ID | tail -n 1`

powerfun convert_analysis -j $JSON_FILE -od $RESULT_DIR