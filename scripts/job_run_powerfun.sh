#!/bin/bash
#SBATCH --job-name=job_powerfun         # Job name
#SBATCH --output=../output/powerfun-%A_%a.out
#SBATCH -e ../error/powerfun_%x.%N.%j.err
#SBATCH --array=1-7%7 # job array index # or -t 1-3%3
#SBATCH --cpus-per-task=1 # Request that ncpus be allocated per process.
#SBATCH --mem-per-cpu=5gb
#SBATCH --time=5:00:00                 # Time limit hrs:min:sec
#SBATCH --mail-user=chabname.ghasseminedjad@inria.fr         #Receive email on this adress when the job is begin,over,or fail 
#SBATCH --mail-type=END,FAIL                  #Define what we want to receive by email about the job statut
#SBATCH --exclude=arm01

source /home/cghassem/miniconda3/etc/profile.d/conda.sh
conda activate powerfun

# Cluster
#./run_powerfun.sh -b ../datas/sbml/ -p ../datas/gut_target_pg_bbl/ -s ../datas/seeds_gut.sbml -g ../datas/gut_genome_taxonomy.tsv -t ../temps/ -r ../results/ -y reaction -a ../datas/targets/
#./run_powerfun.sh -b ../datas/sbml/ -p ../datas/gut_target_pg_bbl/ -s ../datas/seeds_gut.sbml -g ../datas/gut_genome_taxonomy.tsv -t ../temps/ -r ../results/ -y metabolite -a ../datas/targets/
#./run_powerfun.sh -b ../datas/sbml/ -p ../datas/gut_target_pg_bbl/ -s ../datas/seeds_gut.sbml -g ../datas/gut_genome_taxonomy.tsv -t ../temps/ -r ../results/ -y metareact -a ../datas/targets/
./run_powerfun.sh -b ../datas/sbml/ -p ../datas/gut_target_pg_bbl/ -s ../datas/seeds_gut.sbml -g ../datas/gut_genome_taxonomy.tsv -t ../temps/ -r ../results/analysis/ -y analysis -a ../datas/targets/

# Local
#./run_powerfun.sh -b ../data/sbml/ -p ../data/results_gut_targets_pg/gut_target_pg_bbl/ -s ../data/gut_seeds.sbml -g ../data/gut_genome_taxonomy.tsv -t ../temp/ -r ../result/ -y reaction  -a ../data/targets/